# cbfrun #
This is a repository for the interpreter and debugger of the Cleverer Brainfuck, an experimental programming language by Marcel Fiala, implemented as a part of his Bachelor's thesis eventually graded A.

**This is probably the finest piece of code you can find in my public repositories. And it's nicely commented too :-P**

## Thesis abstract ##

In this thesis, an author discusses and analyzes design ﬂaws of experimental programming language Brainfuck, for which he suggests a solution in a form of an extension of original language. Then he formally deﬁnes this extension and implements its interpret and debugger.

Thesis itself (CZ): http://data.marcel-fiala.eu/MarcelFiala_Thesis.pdf