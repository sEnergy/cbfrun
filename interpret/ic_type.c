/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdlib.h>
#include <assert.h>

#include "ic_type.h"
#include "errors.h"
#include "instructions.h"

/*
 * Initializes new innec code data structure and returns pointer to it.
 *
 * Returns NULL on failure.
 */
TInnerCodePtr innerCodeInit (void)
{
    TInnerCodePtr new = malloc(sizeof(TInnerCode));

    if (new != NULL)
    {
        new->first = new->last = new->active = new->bodyStart = NULL;
    }

    return new;
}

/*
 * Adds specified instruction in the end of Inner Code passed as first argument.
 * 
 * Returns zero on success, nonzero error code on failure.
 */
int addInstruction (TInnerCodePtr ic, TInstructionPtr instruction)
{
    TInstructionPtr new = malloc(sizeof(TInstruction));
    
    if (new != NULL) // malloc successful
    {
        new->arg = instruction->arg; 
        new->id = instruction->id;
        new->line = instruction->line;
        new->next = NULL;
        
        // adding new item to given inner code ADS
        if (ic->first == NULL) // empty
        {
            ic->first = ic->last = ic->active = new;
        }
        else
        {
            ic->last->next = new;
            ic->last = new;
        }
        
        return 0;
        
    }
    else // alloc error
    {
        return ERR_ALLOC;
    }
}

/*
 * Frees all memory used by inner Code.
 */
void innerCodeDispose (TInnerCodePtr ic)
{
    TInstructionPtr tmp;
    TInstructionPtr tmpDelete;

    // deletion od items, one by one
    while ((tmp = ic->first) != NULL)
    {
        /*
         * Free memory of after instruction arguments. If argument ins link
         * to another instruction, do not free anything.
         */
        if (tmp->arg != NULL && tmp->id != I_CYCLE_END
                && (tmp->id < I_WHILE || tmp->id > I_FOR_BEGIN))
        {
            free (tmp->arg);
            tmp->arg = NULL;
        }
        
        tmpDelete = ic->first;
        ic->first = ic->first->next;
        free(tmpDelete);
    }

    // freeing of memory after token list structure
    free(ic);
    ic = NULL;
}

/* End of token_list.c */
