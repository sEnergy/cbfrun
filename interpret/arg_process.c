/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "arg_process.h"
#include "errors.h"

/*
 * Processes interpreter's arguments and returns code accordingly (either
 * INTERPRETATION/HELP code or some ERR_ code).
 */
int argumentProcess (const int argc, char *argv[], TArgumentsPtr args)
{
    if (argc >= LEGAL_ARGC_MIN && argc <= LEGAL_ARGC_MAX)
    {
        if (!strcmp(argv[1],"-h") || !strcmp(argv[1],"--help"))
        {
            if (argc == 2)
            {
                return CODE_HELP; // state "Print help"
            }
            else
            {
                return ERR_HELP_NON_STANDALONE;
            }
        }
        else // state "Intepreter"
        {
            const unsigned char l = strlen(argv[1]);

            // checking filename (not existence of file!)
            if (l < EXTENSION_LENGTH+1)
            {
                return ERR_ARG; // filename is too short to be legal
            }
            else if (strcmp(argv[1]+l-EXTENSION_LENGTH,".cbf"))
            {
                return ERR_EXTENSION;
            }

            args->sourceFileName = argv[1];
            args->debug = args->pedantic = args->suppress = false;
 
            for (int x = 2; x < argc; ++x)
            {
                if (!strcmp(argv[x],"-debug"))
                {
                    if (args->debug)
                    {
                        return ERR_ARG_REUSE;
                    }
                    else
                    {
                        args->debug = true;   
                    }
                }
                else if (!strcmp(argv[x],"-pedantic"))
                {
                    if (args->pedantic)
                    {
                        return ERR_ARG_REUSE;
                    }
                    else if (args->suppress)
                    {
                        return ERR_ARG_MIX;
                    }
                    else
                    {
                        args->pedantic = true;   
                    }

                }
                else if (!strcmp(argv[x],"-suppress-warnings"))
                {
                    if (args->suppress)
                    {
                        return ERR_ARG_REUSE;
                    }
                    else if (args->pedantic)
                    {
                        return ERR_ARG_MIX;
                    }
                    else
                    {
                        args->suppress = true;   
                    }  
                }
                else
                {
                    return ERR_ARG;
                }
            }

            return CODE_INTERPRETER;
        }
    }
    else // illegal number of arguments
    {
        return ERR_ARGCOUNT;
    }
}

/* End of file arg_process.c */
