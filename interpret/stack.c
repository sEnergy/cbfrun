/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdlib.h>
#include <limits.h>

#include "stack.h"
#include "errors.h"

/*
 * Initializes new stack and returns its pointer.
 *
 * Returns NULL on failure.
 */
TStackPtr stackInit (void)
{
    TStackPtr new = malloc(sizeof(TStack));

    if (new != NULL)
    {
        new->top = NULL;
        new->size = 0;
    }

    return new;
}

/*
 * Pushes new integer item to the top of stack.
 *
 * Returns zero on succes, error code otherwise.
 */
int stackPushInt (TStackPtr stack, const unsigned int value)
{
    TStackItemPtr new = malloc(sizeof(TStackItem));

    if (new != NULL)
    {
        new->content = malloc(sizeof(unsigned int));
        
        if (new->content != NULL)
        {
            *((unsigned int*)new->content) = value;
            new->next = stack->top;
            stack->top = new;

            ++(stack->size);
            return 0;
        }
        else
        {
            free(new);
            return ERR_ALLOC;
        }
    }
    else
    {
        return ERR_ALLOC;
    }
}

/*
 * Returns integer value from top of stack and removes item carrying this value.
 *
 * Warning: Causes SEGMENTATION FAULT when called on empty stack.
 *          May cause error when called on stack of different data type.
 */
unsigned int stackPopInt (TStackPtr stack)
{
    const unsigned int value = *((unsigned int*)stack->top->content);

    TStackItemPtr tmp = stack->top;
    stack->top = stack->top->next;

    --(stack->size);
    
    free(tmp->content);
    free(tmp);

    return value;
}

/*
 * Pushes new Instruction Pointer item to the top of stack.
 *
 * Returns zero on succes, error code otherwise.
 */
int stackPushInstPtr (TStackPtr stack, const TInstructionPtr ptr)
{
    TStackItemPtr new = malloc(sizeof(TStackItem));

    if (new != NULL)
    {
        new->content = malloc(sizeof(TInstructionPtr));
        
        if (new->content != NULL)
        {
            *((TInstructionPtr*)new->content) = ptr;
            new->next = stack->top;
            stack->top = new;

            ++(stack->size);
            return 0;
        }
        else
        {
            free(new);
            return ERR_ALLOC;
        }
    }
    else
    {
        return ERR_ALLOC;
    }
}

/*
 * Returns Instruction Pointer value from top of stack and removes item carrying 
 * this value.
 *
 * Warning: Causes SEGMENTATION FAULT when called on empty stack.
 *          May cause error when called on stack of different data type.
 */
TInstructionPtr stackPopInstPtr (TStackPtr stack)
{
    const TInstructionPtr value = *((TInstructionPtr*)stack->top->content);

    TStackItemPtr tmp = stack->top;
    stack->top = stack->top->next;

    --(stack->size);
    
    free(tmp->content);
    tmp->content = NULL;
    
    free(tmp);

    return value;
}

/*
 * Frees all memory used by stack.
 */
void stackDispose (TStackPtr stack)
{
    TStackItemPtr tmp;

    while ((stack->size)--)
    {
        tmp = stack->top;
        stack->top = stack->top->next;
        
        free(tmp->content);
        free(tmp);
    }

    free(stack);
    stack = NULL;
}

/*
 * Frees all memory used by header of Instruction Pointer stack.
 */
void PtrStackDispose (TStackPtr stack)
{
    TStackItemPtr tmp;

    while ((stack->size)--)
    {
        tmp = stack->top;
        stack->top = stack->top->next;
        
        free(tmp->content);
        tmp->content = NULL;
        
        free(tmp);
    }

    free(stack);
    stack = NULL;
}

/* End of file stack.c */
