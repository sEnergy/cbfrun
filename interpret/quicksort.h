/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef QUICKSORT_H_INCLUDED
#define QUICKSORT_H_INCLUDED

/*
 * Implementation of quicksort for array of unsigned integers using recursive 
 * alogorithm.
 * 
 * This implemenatiton sorts items to ascending order.
 */
void quicksort (unsigned int array[], unsigned int left, unsigned int right);

#endif

/* End of file quicksort.h */
