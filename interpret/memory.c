/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "memory.h"
#include "errors.h"

/*
 * Memory initialization - alloc default size memory, save the size and set
 * pointer and highest pointer to zero.
 *
 * Returns zero on success, ERR_ code otherwise.
 */
int memoryInit (TMemoryPtr m)
{
    m->data = malloc(MEM_DEF_SIZE*sizeof(unsigned int));

    if (m->data != NULL)
    {
        m->ptr = m->highest = 0;
        m->data[m->ptr] = 0;
        m->size = MEM_DEF_SIZE;

        return 0;
    }
    else
    {
        return ERR_ALLOC;
    }
}

/*
 * Increments pointer. If the ponter is new highest adress, initializes value to
 * zero and saves new highest adress. In case ptr is UINT_MAX, only wrap back to
 * zero. Otherwise realloc the memory.
 *
 * Returns zero on success, ERR_ code otherwise.
 */
int incPtr (TMemoryPtr m, unsigned int number)
{
    while (number--)
    {
        ++(m->ptr); // increment pointer

        if (m->highest < m->ptr) // if this is new highest address
        {
            if (m->ptr == m->size) // if current memory is insufficient
            {
                if (m->ptr != UINT_MAX) // buffer size not at max -> reallocate
                {
                    // memory size must me always (2^n)-1
                    m->size = (((m->size)+1)*2)-1;

                    // realloc memory
                    m->data = realloc(m->data, (m->size)*sizeof(unsigned int));

                    if (m->data == NULL)
                    {
                        return ERR_ALLOC;
                    }
                }
                else // if buffer size is at its maximum -> wrap back to 0
                {
                    m->ptr = 0;
                }

            }

            m->highest = m->ptr; // save new highest adress
            m->data[m->ptr] = 0; // initialize new zero
        }
    }

    return 0;
}

/*
 * Decrement pointer. If called from ptr == 0, go to highest used adress.
 */
void decPtr (TMemoryPtr m, unsigned int number)
{
    while (number--)
    {
        if (m->ptr == 0) // from zero to highest used adress
        {
            m->ptr = m->highest;
        }
        else // else just decrement
        {
            --(m->ptr);
        }
    }
}

/* End of memory.c */
