/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef IC_TYPE_H_INCLUDED
#define IC_TYPE_H_INCLUDED

/*
 * Reset pointer to active instruction to first one.
 */
#define ResetActiveInst(ic) ic->active = ic->first

/*
 * Moves active instruction poiner to next instruction.
 */
#define ShiftActiveInstr(ic) ic->active = ic->active->next

/*
 * Single item of Token List. Points to next item only.
 */
typedef struct TInstruction {
    unsigned int line;
    unsigned char id;
    void *arg;
    struct TInstruction* next;
} TInstruction;

/*
 * Token list implemented as single-linked linear list.
 */
typedef struct TInnerCode {
    struct TInstruction *first, *last, *active, *bodyStart;
} TInnerCode;

typedef TInnerCode* TInnerCodePtr;
typedef TInstruction* TInstructionPtr;

/*
 * Initializes new innec code data structure and returns pointer to it.
 *
 * Returns NULL on failure.
 */
TInnerCodePtr innerCodeInit (void);

/*
 * Adds specified instruction in the end of Inner Code passed as first argument.
 * 
 * Returns zero on success, nonzero error code on failure.
 */
int addInstruction (TInnerCodePtr ic, TInstructionPtr instruction);

/*
 * Frees all memory used by inner Code.
 */
void innerCodeDispose (TInnerCodePtr ic);

#endif

/* End of file ic_type.h */
