/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <limits.h>

#include "scanner.h"
#include "terminals.h"
#include "errors.h"

/*
 * Implementation of Scanner - Lexical analyzer. On each call, Scanner reads
 * one token from given source file. Also, Scanner updates info about posiniton
 * within source file, stored as row&columb value in TSource structure.
 * 
 * If everything is OK, token ID and token content (if any) is stored in
 * token structure passed as argument and zero value is returned.
 * 
 * On error, both token ID and content are zero/NULL, lex error position is
 * stored in errDetail structure and nonzero error code is returned.
 */
int readNextToken (TSourcePtr src, TTokenPtr token, TErrorDetailPtr errDetail)
{
    token->content = NULL;
    token->id = 0;
    
    char c, tmp; // current character & temponary variable
    
    while (1)
    {
        c = fgetc(src->source);
        ++(src->c);
        
        switch (c)
        {
            case '\n':
                ++(src->r);
                src->c = 0;
                break;
            
            case EOF:
                token->id = T_EOF;
                return 0;
                break;
            
            case '@':
                token->id = T_MODULATOR;
                return 0;
                break;
            
            case '#':
                token->id = T_SHARP;
                return 0;
                break;
            
            case '{':
                token->id = T_CURLYLEFT;
                return 0;
                break;
            
            case '}':
                token->id = T_CURLYRIGHT;
                return 0;
                break;
            
            case '[':
                token->id = T_BRACKETLEFT;
                return 0;
                break;
            
            case ']':
                token->id = T_BRACKETRIGHT;
                return 0;
                break;
            
            case '(':
                token->id = T_PARENLEFT;
                return 0;
                break;
            
            case ')':
                token->id = T_PARENRIGHT;
                return 0;
                break;
            
            case '$':
                token->id = T_DOLLAR;
                return 0;
                break;
                
            case '|':
                token->id = T_DEBUG;
                return 0;
                break;
            
            case ',':
                token->id = T_GETCHAR;
                return 0;
                break;
            
            case '.':
                token->id = T_PUTCHAR;
                return 0;
                break;
            
            case ':':
                token->id = T_PUTINT;
                return 0;
                break;
            
            case ';':
                token->id = T_PUTSTRING;
                return 0;
                break;
            
            case '/':
                token->id = T_PUSH;
                return 0;
                break;
            
            case '\\':
                token->id = T_POP;
                return 0;
                break;
            
            case '?':
                token->id = T_TOP;
                return 0;
                break;
            
            case '!':
                token->id = T_REMOVE;
                return 0;
                break;
            
            case '&':
                token->id = T_SAVEADDR;
                return 0;
                break;
            
            case '*':
                token->id = T_LONGJUMP;
                return 0;
                break;
            
            case 'A':
                token->id = T_ADD;
                return 0;
                break;
            
            case 'S':
                token->id = T_SUBTRACT;
                return 0;
                break;
            
            case 'M':
                token->id = T_MULTIPLY;
                return 0;
                break;
            
            case 'D':
                token->id = T_DEVIDE;
                return 0;
                break;
            
            case 'E':
                token->id = T_EMPTY;
                return 0;
                break;
            
            case 'H':
                token->id = T_TOPINC;
                return 0;
                break;
            
            case 'L':
                token->id = T_TOPDEC;
                return 0;
                break;
            
            case '+':
                token->id = T_VALINC;
                return 0;
                break;
            
            case '-':
                token->id = T_VALDEC;
                return 0;
                break;
            
            case '>':
                token->id = T_ADDRINC;
                return 0;
                break;
            
            case '<':
                token->id = T_ADDRDEC;
                return 0;
                break;
            
            case '_':
                token->id = T_RESET;
                return 0;
                break;
            
            case '%':
                token->id = T_SORT;
                return 0;
                break;
            
            case '^':
                token->id = T_RETURN;
                return 0;
                break;
            
            case '=': // comment -> skip
            
                while (((c = fgetc(src->source)) != EOF) && c != '\n');
                ungetc(c, src->source);
                
                ++(src->r); src->c = 0; // set position to next line
                
                break;
                
            case '\'': // char / int
                
                src->c += 2;
                tmp = fgetc(src->source);
                
                // apostrophe - standard character - apostrophe
                if ((c = fgetc(src->source)) == '\'')
                {
                    token->content = calloc(2, sizeof(char));
                    token->content[0] = tmp;
                    token->id = T_CHAR;
                    
                    return 0;
                }
                // apostrophe - newline escape sequence - apostrophe
                else if (tmp == '\\' && c == 'n')
                {
                    ++(src->c);
                    if ((c = fgetc(src->source)) == '\'')
                    {
                        token->content = calloc(2, sizeof(char));
                        token->content[0] = '\n';
                        token->id = T_CHAR;
                        
                        return 0;
                    }
                    else
                    {
                        errDetail->line = src->r;
                        errDetail->columb = src->c;
                        
                        return ERR_LEXICAL;
                    }
                }
                else if (tmp == 'd') // decimal number
                {
                    ungetc(c, src->source);
                    --(src->c);
                    
                    return scanDecInteger (src, token, errDetail);
                }
                else if (tmp == 'b') // binary number
                {
                    ungetc(c, src->source);
                    --(src->c);
                    
                    return scanBinInteger (src, token, errDetail);
                }
                else
                {
                    errDetail->line = src->r;
                    errDetail->columb = src->c;
                    
                    return ERR_LEXICAL;
                }
                
                break;
                
            case '"':
                return scanString (src, token, errDetail);
                break;
                
            default:
                
                if (c < '!' || c > '~') // ingored characters
                {
                    continue;
                }
                if (isdigit(c)) // single digits (for short jumps)
                {
                    token->content = calloc(2, sizeof(char));
                    token->content[0] = c;
                    token->id = T_DIGIT;
                    
                    return 0;
                }
                else if (c >= 'a' && c <= 'z') // procedure IDs
                {
                    token->content = calloc(2, sizeof(char));
                    token->content[0] = c;
                    token->id = T_ID;
                    
                    return 0;
                }
                else // invalid characters
                {
                    errDetail->line = src->r;
                    errDetail->columb = src->c;
                
                    return ERR_LEX_INVALID;
                }
                
                break;
        }
        
    }
}

/*
 * Function called when quotation mark is found - means there is string to read.
 * String consits of printable characters and/or \n \" escape sequences.
 * 
 * If everything is OK, token ID is set to T_STRING and token content pointer is
 * set to point to allocated memory with string and zero value is returned.
 * 
 * On error, both token ID and content are undefined, lex error position is
 * stored in errDetail structure and nonzero error code is returned. 
 */
int scanString (TSourcePtr src, TTokenPtr token, TErrorDetailPtr errDetail)
{
    unsigned long int size = DEFAULT_STRING_LENGTH, i = 0;
    char c;
    char* string = calloc(size, sizeof(char));

    while (((c = fgetc(src->source)) != EOF) && c != '"')
    {
        ++(src->c);
        
        if (i == size) // if capacity of buffer is filed
        {
            size *= 2; // double the size

            // realloc memory for source code
            string = realloc(string, size*sizeof(unsigned char));

            if (string == NULL)
            {
                return ERR_ALLOC;
            }
        }
        
        // newline -> update stored postion within source file
        if (c == '\n')
        {
            ++(src->r);
            src->c = 0;
        }

        if (c == '\\') // possible start of escape sequence
        {
            ++(src->c);
            
            if ((c = fgetc(src->source)) == 'n') // \n
            {
                string[i++] = '\n';
            }
            else if (c == '"') // \"
            {
                string[i++] = '"';
            }
            else // just \ - normal character
            {
                string[i++] = '\\';
                ungetc(c, src->source);
                
                --(src->c);
            }
        }
        else // normal character
        {
            string[i++] = c;
        }
    }
    
    ++(src->c);
    
    if (c == EOF) // premature end of string
    {
        free(string);
        
        errDetail->line = src->r;
        errDetail->columb = src->c;
    
        return ERR_LEX_STRING;
    }
    else // everything OK -> put ending character in the end of loaded string
    {
        if (i == size) // if capacity of buffer is filled
        {
            // realloc memory for source code
            string = realloc(string, (size+1)*sizeof(unsigned char));

            if (string == NULL)
            {
                return ERR_ALLOC;
            }
        }

        string[i++] = '\0'; // close the string
    }

    token->content = string;
    token->id = T_STRING;

    return 0;
}

/*
 * Function called when apostrophe and ower-case letter D are read, which means
 * start of decinal integer.
 * 
 * If everything is OK, token ID is set to T_INT and token content pointer is
 * set to point to allocated memory with letter d and string of scanned digits.
 * 
 * On error, both token ID and content are undefined, lex error position is
 * stored in errDetail structure and nonzero error code is returned. 
 */
int scanDecInteger (TSourcePtr src, TTokenPtr token, TErrorDetailPtr errDetail)
{
    unsigned char digits, c, i = 0;
    char* number; // buffer for loading number representation
    
    // calculating max number of valid digits for current architecture
    digits = floor(log10(UINT_MAX))+1;

    // allocated memory for number representation
    if ((number = calloc(digits+2, sizeof(char))) == NULL)
    {
        return ERR_ALLOC;
    }
    
    number[i++] = 'd';

    // skip zeros at the beginning
    while ((c = fgetc(src->source)) == '0') ++(src->c);
    
    if (c == '\'') // number is zero
    {
        number[i++] = '0';
    }
    else
    {
        do // reading digits
        {
            ++(src->c);
            
            if (i == digits+1) // number is too big for UINT
            {
                number[i] = '\0';
                
                errDetail->line = src->r;
                errDetail->columb = src->c;
                errDetail->data = number;
                
                return ERR_BIG_NUMBER;
            }

            if (isdigit(c))
            {
                number[i++] = c;
            }
            else // unallowed character - only digits are valid
            {
                free(number);
                
                errDetail->line = src->r;
                errDetail->columb = src->c;
                
                return ERR_LEX_DIGIT;
            }
        } while ((c = fgetc(src->source)) != '\'');
    }
    
    ++(src->c);
    
    number[i] = '\0'; // end string (number representation)
    
    token->id = T_INT;
    token->content = number;
    
    return 0;
}

/*
 * Function called when apostrophe and ower-case letter B are read, which means
 * start of binary integer.
 * 
 * If everything is OK, token ID is set to T_INT and token content pointer is
 * set to point to allocated memory with letter d and string of scanned digits.
 * 
 * On error, both token ID and content are undefined, lex error position is
 * stored in errDetail structure and nonzero error code is returned. 
 */
int scanBinInteger (TSourcePtr src, TTokenPtr token, TErrorDetailPtr errDetail)
{
    unsigned char digits, c, i = 0;
    char* number; // buffer for loading number representation
    
    // max number of valid digits for current architecture
    digits = BINARY_DIGITS;

    // allocated memory for number representation
    if ((number = calloc(digits+2, sizeof(char))) == NULL)
    {
        errDetail->line = src->r;
        errDetail->columb = src->c;

        return ERR_ALLOC;
    }
    
    number[i++] = 'b';

    // skip zeros at the beginning
    while ((c = fgetc(src->source)) == '0') ++(src->c);
    
    if (c == '\'') // number is zero
    {
        number[i++] = '0';
    }
    else
    {
        do // reading digits
        {
            ++(src->c);
            
            if (i == digits) // number is too big for UINT
            {
                number[i] = '\0';
                
                errDetail->line = src->r;
                errDetail->columb = src->c;
                errDetail->data = number;
                
                return ERR_BIG_NUMBER;
            }

            if (c == '0' || c == '1')
            {
                number[i++] = c;
            }
            else // unallowed character - only binary digits are valid
            {
                free(number);
                
                errDetail->line = src->r;
                errDetail->columb = src->c;
                
                return ERR_LEX_DIGIT;
            }
        } while ((c = fgetc(src->source)) != '\'');
    }
    
    ++(src->c);
    
    number[i] = '\0'; // end string (number representation)
    
    token->id = T_INT;
    token->content = number;
    
    return 0;
}

/* End of scanner.c */
