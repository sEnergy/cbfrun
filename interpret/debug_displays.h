/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef DEBUG_DISPLAYS_H_INCLUDED
#define DEBUG_DISPLAYS_H_INCLUDED

#include "stack.h"
#include "memory.h"

/*
 * Shows current state o interpreted's program stack in pseudo-graphic output.
 */
void displayStack (const TStackPtr stack);

/*
 * Shows current state o interpreted's program memory in pseudo-graphic output.
 */
void displayMemory (const TMemoryPtr m);


#endif

/* End of file debug_displays.h */
