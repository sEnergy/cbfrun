/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef NONTERMINALS_H_INCLUDED
#define NONTERMINALS_H_INCLUDED

/*
 * Value representing nonterminal with lowest values - must be chosen high
 * enough not to collide with terminal IDs.
 */
#define NT_DEFAULT 42

/*
 * Definifiton of nonterminal IDs.
 */
enum nonterminals
{
    NT_PROGRAM = NT_DEFAULT,
    NT_PROCDEFSCOPE,
    NT_PROCDEFLIST,
    NT_PROCDEF,
    NT_STATEMENTLIST,
    NT_STATEMENT,
    NT_ADDRESSCHANGE,
    NT_ADDRESSCHANGEARG,
    NT_MODULABLECONSTR,
    NT_MODULATOR,
    NT_MODULABLESTATEMENT,
    NT_INSTRUCTION
};

#endif

/* End of file nonterminals.h */
