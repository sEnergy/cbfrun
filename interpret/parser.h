/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED

#include "arg_type.h"
#include "token_list.h"
#include "errors.h"

/*
 * Syntax analyzer - parser implementing predictive algorith using LL parsing 
 * table, expansion rules and stack.
 * 
 * When source file whose name is saved in structure args, all loaded tokens
 * are save to passed tokenList and zero value is returned. Otherwise
 * nonzero value (error code) is returned and info about error that occured
 * is save to structure arrorDetail.
 */
int parserCore (TArgumentsPtr args, TTokenListPtr tokenList, TErrorDetailPtr errorDetail);

#endif

/* End of file parser.h */
