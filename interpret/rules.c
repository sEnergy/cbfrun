/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <string.h>

#include "rules.h"
#include "terminals.h"
#include "nonterminals.h"

/*
 * Function that initializes array of rules for parser.
 */
void rulesInit (TRules dest)
{
    /*
     * Grammar rules for language lecRAM. 
     * 
     * 0 represents end of rule
     * 
     * PLEASE NOTE THAT TERMINALS/NONTERMIMALS IN RULES ARE WRITTEN
     * IN REVERSED ORDER !!!
     */
    TRules src = {
    { T_EOF, NT_STATEMENTLIST, NT_PROCDEFSCOPE, 0 },                            // 1
    { 0 },                                                                      // 2
    { T_DOLLAR, NT_PROCDEFLIST, T_DOLLAR, 0 },                                  // 3
    { NT_PROCDEFLIST, NT_PROCDEF, 0 },                                          // 4
    { T_CURLYRIGHT, NT_STATEMENTLIST, T_CURLYLEFT, T_ID, 0},                    // 5
    { 0 },                                                                      // 6
    { NT_STATEMENTLIST, NT_STATEMENT, 0 },                                      // 7
    { NT_INSTRUCTION, 0},                                                       // 8
    { NT_ADDRESSCHANGE, 0},                                                     // 9
    { T_BRACKETRIGHT, NT_STATEMENTLIST, T_BRACKETLEFT, T_INT, T_SHARP, 0 },     // 10
    { NT_MODULABLECONSTR, 0},                                                   // 11
    { NT_ADDRESSCHANGEARG, T_ADDRINC, 0 },                                      // 12
    { NT_ADDRESSCHANGEARG, T_ADDRDEC, 0 },                                      // 13 
    { 0 },                                                                      // 14
    { T_DIGIT, 0 },                                                             // 15
    { NT_MODULABLESTATEMENT, NT_MODULATOR, 0},                                  // 16 
    { 0 },                                                                      // 17
    { T_MODULATOR, 0 },                                                         // 18
    { T_STRING, 0},                                                             // 19
    { T_PARENRIGHT, NT_STATEMENTLIST, T_PARENLEFT, 0 },                         // 20
    { T_BRACKETRIGHT, NT_STATEMENTLIST, T_BRACKETLEFT, 0 },                     // 21
    { T_DEBUG, 0 },                                                             // 22
    { T_GETCHAR, 0 },                                                           // 23
    { T_PUTCHAR, 0 },                                                           // 24
    { T_PUTINT, 0 },                                                            // 25
    { T_PUTSTRING, 0 },                                                         // 26
    { T_PUSH, 0 },                                                              // 27
    { T_POP, 0 },                                                               // 28
    { T_TOP, 0 },                                                               // 29
    { T_REMOVE, 0 },                                                            // 30
    { T_SAVEADDR, 0 },                                                          // 31
    { T_LONGJUMP, 0 },                                                          // 32
    { T_ADD, 0 },                                                               // 33
    { T_SUBTRACT, 0 },                                                          // 34
    { T_DEVIDE, 0 },                                                            // 35
    { T_MULTIPLY, 0 },                                                          // 36
    { T_TOPINC, 0 },                                                            // 37
    { T_TOPDEC, 0 },                                                            // 38 
    { T_EMPTY, 0 },                                                             // 39
    { T_RESET, 0 },                                                             // 40
    { T_ID, 0 },                                                                // 41
    { T_CHAR, 0 },                                                              // 42
    { T_INT, 0 },                                                               // 43
    { T_RETURN, 0 },                                                            // 44
    { T_VALINC, 0 },                                                            // 45 
    { T_VALDEC, 0 },                                                            // 46
    { T_SORT, 0 },                                                              // 47
    { 0 },                                                                      // 48
    };
   
  /* 
   * Table was initialized as local variable, so memcpy is needed to copy table
   * data to array passed as argument of function.
   */
  memcpy(dest, src, RULES_NUMBER*MAX_RULE_LENGTH);
}

/* End of rules.c */
