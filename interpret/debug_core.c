/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug_core.h"
#include "debug_displays.h"

/*
 * Core ofdebugger - accepts user's commands and calls debug display functions.
 */
int debugCore (const TMemoryPtr m, const TStackPtr stack, unsigned int line)
{
	// return code - continue, error or terminate
	int code = CONTINUE;

	// input buffer
	char *in = calloc(sizeof(char), LEN+1);

	while(42)
	{
		memset(in, 0, sizeof(char)*LEN+1); // buffer reset

		fprintf(stderr, "> ");
		fgets (in, LEN, stdin);

		int len = strlen(in);

		// clean input
		if (len == LEN)
			while (getchar() != '\n' );

		// split input to words
		for (unsigned char i = 0; in[i]; ++i)
		{
			if (in[i] == ' ' || in[i] == '\n')
			{
				in[i] = '\0';
			}
		}

		unsigned int state = STATE_START, setTarget, cell;

		// command processing
		for (char *c = in; c < in+len; c += strlen(c)+1)
		{
			if (state == STATE_START)
			{
				if (!strcmp(c, "continue"))
				{
					goto continue_interpretation;
				}
				else if (!strcmp(c, "terminate"))
				{
					code = TERMINATE;
					goto continue_interpretation;
				}
				else if (!strcmp(c, "help"))
				{
					debugHelp();
					goto read_next_command;
				}
				else if (!strcmp(c, "stack"))
				{
					displayStack(stack);
					goto read_next_command;
				}
				else if (!strcmp(c, "memory"))
				{
					displayMemory(m);
					goto read_next_command;
				}
				else if (!strcmp(c, "set"))
				{
					state = STATE_SET;
				}
				else if (!strcmp(c, "push"))
				{
					setTarget = TARGET_PUSH;
					state = STATE_VALUE;
				}
				else if (!strcmp(c, "pop"))
				{
					stackPopInt(stack);
				}
				else if (!strcmp(c, "line"))
				{
					fprintf(stderr, "\n>> number of current line is: %d\n\n", line);
				}
				else
				{
					MsgBadCommand();
					goto read_next_command;
				}
			}
			else if (state == STATE_SET)
			{
				if (!strcmp(c, "cell"))
				{
					setTarget = TARGET_CELL;
					state = STATE_CELLNUMBER;
				}
				else if (!strcmp(c, "stacktop"))
				{
					if (StackEmpty(stack))
					{
						MsgEmptyStack();
						goto read_next_command;
					}

					setTarget = TARGET_STACK;
					state = STATE_TO;
				}
				else if (!strcmp(c, "address"))
				{
					setTarget = TARGET_ADDRESS;
					state = STATE_TO;
				}
				else
				{
					MsgBadCommand();
					goto read_next_command;
				}
			}
			else if (state == STATE_TO)
			{
				if (!strcmp(c, "to"))
				{
					state = STATE_VALUE;
				}
				else
				{
					MsgBadCommand();
					goto read_next_command;
				}
			}
			else if (state == STATE_VALUE)
			{
				char *err;
				unsigned long int value = strtoll(c, &err, 10);

				if (strlen(err))
				{
					MsgBadValue();
					goto read_next_command;
				}

				if (setTarget == TARGET_ADDRESS)
				{
					if (value > m->highest)
					{
						MsgHighAddress();
						goto read_next_command;
					}

					m->ptr = value;
				}
				else if (setTarget == TARGET_STACK)
				{
					if (stack->size)
					{
						StackTopInt(stack) = value;
					}
					else
					{
						MsgEmptyStack();
						goto read_next_command;
					}
				}
				else if (setTarget == TARGET_PUSH)
				{
					stackPushInt(stack, value);
				}
				else
				{
					m->data[cell] = value;
				}
			}
			else
			{
				char *err;
				cell = strtol(c, &err, 10);

				if (strlen(err))
				{
					MsgBadAddress();
					goto read_next_command;
				}
				else if (cell > m->highest)
				{
					MsgHighAddress();
					goto read_next_command;
				}
				else
				{
					state = STATE_TO;
				}
			}
		}

		read_next_command:
			continue;
	}

	continue_interpretation:
		free(in);
		return code;
}

/*
 * Prints message at start of debugger.
 */
void debugStartMessage (char* sourceFileName)
{
	fprintf(stderr,   "\n---------------------------------------------------------\n");
	fprintf(stderr,   "--------------------- Starting debug --------------------\n");
	fprintf(stderr,   "---------------------------------------------------------\n\n");
	fprintf(stderr,   "source file name: %s\n\n", sourceFileName);
	fprintf(stderr,   "For list of avilable commands, use 'help'.\n\n");
}

/*
 * Prints message at termination of debugger.
 */
void debugEndMessage (void)
{
	fprintf(stderr,   "\n---------------------------------------------------------\n");
	fprintf(stderr,   "-------------------- Debug terminated -------------------\n");
	fprintf(stderr,   "---------------------------------------------------------\n\n");
}


/*
 * Prints message debug help.
 */
void debugHelp(void)
{
	fprintf(stderr,   "\n>> available commands:"
					  "\n>>      continue             goto next breakpoint"
					  "\n>>      terminate            end interpreter"
					  "\n>>      memory               print memory state"
					  "\n>>      stack                print stack state"
					  "\n>>      set address to X     set address to value X"
					  "\n>>      set cell X to Y      set cell with address X to value Y"
					  "\n>>      set stacktop to X    set item at stacktop to value X"
					  "\n>>      push X               push item with value X to stack"
					  "\n>>      pop                  remove item from stack"
					  "\n>>      help                 print this text once again\n\n");

}

/* End of file debug_core.c */
