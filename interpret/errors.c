/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "errors.h"
#include "terminals.h"

/*
 * Prints error message on stderr based on given error code
 */
void errPrint (const int code, const char *sourceFileName, TErrorDetailPtr err)
{
    char* error;

    switch (code)
    {
        case ERR_ARGCOUNT:
            error = "Illegal number of arguments.";
            break;

        case ERR_ARG:
            error = "Incorrect interpreter argument.";
            break;

        case ERR_ARG_REUSE:
            error = "Cannot use same argument multiple times.";
            break;

        case ERR_ARG_MIX:
            error = "Cannot use both -pedantic and -suppress-warnings at the same time.";
            break;

        case ERR_HELP_NON_STANDALONE:
            error = "Help parameter must be used as the only parameter.";
            break;

        case ERR_EXTENSION:
            error = "No or incorrect extensison of source file (must be .cbf).";
            break;

        case ERR_NOFILE:
            error = "Intepreter could not open specified source file.";
            break;
            
        case ERR_LEXICAL:
            error = "Lexical error during read of source file.";
            break;
            
        case ERR_ALLOC:
            error = "Error during allocation of memory.";
            break;
            
        case ERR_LEX_INVALID:
            error = "Lexical error - invalid character.";
            break;
            
        case ERR_LEX_STRING:
            error = "Lexical error - premature end of string token.";
            break;
            
        case ERR_LEX_DIGIT:
            error = "Lexical error - invalid character in integer token.";
            break;

        default:
            error = "Undefined error.";
            break;
    }
    
    if (code == ERR_BIG_NUMBER)
    {
        if (err->columb == UINT_MAX)
        {
            fprintf(stderr, "\nError %d @ %s(l:%u):\n%s '%s' %s.\n%s",
                    code, sourceFileName, err->line, "Parse error -", 
                    err->data, "is too big number for current architecture", 
                    "If you need help, run program with parameter [-h|--help].\n");
        }
        else
        {
            fprintf(stderr, "\nError %d @ %s(l:%u c:%u):\n%s '%s' %s.\n%s",
                    code, sourceFileName, err->line, err->columb, "Parse error -", 
                    err->data, "is too big number for current architecture", 
                    "If you need help, run program with parameter [-h|--help].\n");  
        }  
                      
        free (err->data);
    }
    else if (code == ERR_UNEXPECTED_TOKEN)
    {
        if (err->tokenID == T_STRING)
        {
            fprintf(stderr, "\nError %d @ %s(l:%u c:%u):\n%s '\"%s\"'.\n%s",
                code, sourceFileName, err->line, err->columb, 
                "Syntax error - unexpected token", err->data,
                "If you need help, run program with parameter [-h|--help].\n");  
                
            free (err->data);
        }
        else if (err->tokenID == T_INT || err->tokenID == T_CHAR)
        {
            fprintf(stderr, "\nError %d @ %s(l:%u c:%u):\n%s ''%s''.\n%s",
                code, sourceFileName, err->line, err->columb, 
                "Syntax error - unexpected token", err->data,
                "If you need help, run program with parameter [-h|--help].\n");  
                
            free (err->data);
        }
        else if (err->tokenID == T_DIGIT || err->tokenID == T_ID)
        {
            fprintf(stderr, "\nError %d @ %s(l:%u c:%u):\n%s '%s'.\n%s",
                code, sourceFileName, err->line, err->columb, 
                "Syntax error - unexpected token", err->data,
                "If you need help, run program with parameter [-h|--help].\n");  
                
            free (err->data);
        }
        else
        {
            switch (err->tokenID)
            {
                case T_EOF:
                    err->data = "EOF";
                    break;
                    
                case T_MODULATOR:
                    err->data = "@";
                    break;
                    
                case T_SHARP:
                    err->data = "#";
                    break;
                    
                case T_CURLYLEFT:
                    err->data = "{";
                    break;
                    
                case T_CURLYRIGHT:
                    err->data = "}";
                    break;
                    
                case T_BRACKETLEFT:
                    err->data = "[";
                    break;
                    
                case T_BRACKETRIGHT:
                    err->data = "]";
                    break;
                    
                case T_PARENLEFT:
                    err->data = "(";
                    break;
                    
                case T_PARENRIGHT:
                    err->data = ")";
                    break;
                    
                case T_DEBUG:
                    err->data = "|";
                    break;
                    
                case T_DOLLAR:
                    err->data = "$";
                    break;
                    
                case T_GETCHAR:
                    err->data = ",";
                    break;
                    
                case T_PUTCHAR:
                    err->data = ".";
                    break;
                    
                case T_PUTINT:
                    err->data = ":";
                    break;
                    
                case T_PUTSTRING:
                    err->data = ";";
                    break;
                    
                case T_PUSH:
                    err->data = "/";
                    break;
                    
                case T_POP:
                    err->data = "\\";
                    break;
                    
                case T_TOP:
                    err->data = "?";
                    break;
                    
                case T_REMOVE:
                    err->data = "!";
                    break;
                    
                case T_SAVEADDR:
                    err->data = "&";
                    break;
                    
                case T_LONGJUMP:
                    err->data = "*";
                    break;
                    
                case T_ADD:
                    err->data = "A";
                    break;
                    
                case T_SUBTRACT:
                    err->data = "S";
                    break;
                    
                case T_MULTIPLY:
                    err->data = "M";
                    break;
                    
                case T_DEVIDE:
                    err->data = "D";
                    break;
                    
                case T_EMPTY:
                    err->data = "E";
                    break;
                    
                case T_TOPINC:
                    err->data = "H";
                    break;
                    
                case T_TOPDEC:
                    err->data = "L";
                    break;
                    
                case T_VALINC:
                    err->data = "+";
                    break;
                    
                case T_VALDEC:
                    err->data = "-";
                    break;
                    
                case T_ADDRINC:
                    err->data = ">";
                    break;
                    
                case T_ADDRDEC:
                    err->data = "<";
                    break;
                    
                case T_RESET:
                    err->data = "_";
                    break;
                    
                case T_SORT:
                    err->data = "%";
                    break;
                    
                case T_RETURN:
                    err->data = "^";
                    break;
            }
            
            fprintf(stderr, "\nError %d @ %s(l:%u c:%u):\n%s '%s'.\n%s",
                code, sourceFileName, err->line, err->columb, 
                "Syntax error - unexpected token", err->data,
                "If you need help, run program with parameter [-h|--help].\n");
        }
    }
    else if (code == ERR_PROC_REDEF)
    {
        fprintf(stderr, "\nError %d @ %s(l:%u):\n%s '%c'. %s %d.\n%s",
                code, sourceFileName, err->line, 
                "Redefinition of procedure", err->data[0],
                "Previously defined at line", err->tokenID,
                "If you need help, run program with parameter [-h|--help].\n");  
                
        free(err->data);
    }
    else if (code == ERR_PROC_UNDEF)
    {
        fprintf(stderr, "\nError %d @ %s(l:%u):\n%s '%c'.\n%s",
            code, sourceFileName, err->line, 
            "Call of undefined procedure", err->tokenID,
            "If you need help, run program with parameter [-h|--help].\n");
    }
    else if (code == ERR_DEVIDE_BY_ZERO)
    {
        fprintf(stderr, "\nError %d @ %s(l:%u):\n%s.\n%s",
            code, sourceFileName, err->line, 
            "Devision by zero",
            "If you need help, run program with parameter [-h|--help].\n");
    }
    else if (code > ERR_NOFILE)
    {
        fprintf(stderr, "\nError %d @ %s(l:%u c:%u):\n%s\n%s",
            code, sourceFileName, err->line, err->columb, error,
            "If you need help, run program with parameter [-h|--help].\n");
    }
    else
    {
        fprintf(stderr, "\nError %d:\n%s\n%s", code, error,
            "If you need help, run program with parameter [-h|--help].\n");
    }
}

/* End of file errors.c */
