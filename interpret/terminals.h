/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef TERMINALS_H_INCLUDED
#define TERMINALS_H_INCLUDED

/*
 * Definifiton of terminal IDs.
 */
enum terminals
{
    T_EOF = 1,      // end of file
    T_MODULATOR,    // @
    T_SHARP,        // #
    T_CURLYLEFT,    // {
    T_CURLYRIGHT,   // }
    T_BRACKETLEFT,  // [
    T_BRACKETRIGHT, // ]
    T_PARENLEFT,    // (
    T_PARENRIGHT,   // )
    T_INT,          // RE: '(b(1+0)*)+(d(0..9)*)'      <-- 10th
    T_CHAR,         // 'a..z'
    T_DIGIT,        // 0..9
    T_ID,           // a..z
    T_STRING,       // "(0..9+a..z+<special chars>)*"
    T_DEBUG,        // |
    T_DOLLAR,       // $
    T_GETCHAR,      // ,
    T_PUTCHAR,      // .
    T_PUTINT,       // :
    T_PUTSTRING,    // ;        <-- 20th
    T_PUSH,         // /        
    T_POP,          /* \  */
    T_TOP,          // ?
    T_REMOVE,       // !
    T_SAVEADDR,     // &
    T_LONGJUMP,     // *
    T_ADD,          // A
    T_SUBTRACT,     // S
    T_MULTIPLY,     // M
    T_DEVIDE,       // D        <-- 30th
    T_EMPTY,        // E
    T_TOPINC,       // H
    T_TOPDEC,       // L
    T_VALINC,       // +
    T_VALDEC,       // -
    T_ADDRINC,      // >
    T_ADDRDEC,      // <
    T_RESET,        // _
    T_SORT,         // %
    T_RETURN        // ^        <-- 40th
};

#endif

/* End of file terminals.h */
