/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef WARNINGS_H_INCLUDED
#define WARNINGS_H_INCLUDED

#define STYLE_WARNING 1
#define STYLE_ERROR 0

#define PEDANTIC_WARNING_CODE_DIFF 42

/*
 * Warning codes to clearly identify warning and give user proper info.
 */
enum warningCodes
{
    WARNING_EMPTYSTACK_JUMP = 1,
    WARNING_EMPTYSTACK_GETTOP,
    WARNING_EMPTYSTACK_USETOP,
    WARNING_EMPTYSTACK_REMOVE,
    WARNING_EMPTYSTACK_EDIT,
    WARNING_SORT_FEW,
    WARNING_SORT_UNITIALIZED
};

/*
 * Prints warning message on stderr based on given warning code
 */
void warPrint (const int code, const char *sourceFileName, unsigned int line, const unsigned char style);

#endif

/* End of file errors.h */
