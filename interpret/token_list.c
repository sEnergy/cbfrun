/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdlib.h>

#include "token_list.h"
#include "errors.h"

/*
 * Initializes new token list and returns pointer to it.
 *
 * Returns NULL on failure.
 */
TTokenListPtr tokenListInit (void)
{
    TTokenListPtr new = malloc(sizeof(TTokenList));

    if (new != NULL)
    {
        new->first = new->last = NULL;
    }

    return new;
}

/*
 * Adds specified token in the end of Token List passed as first argument.
 * 
 * Returns zero on success, nonzero error code on failure.
 */
int tokenListAdd (TTokenListPtr tokenList, TTokenPtr token, unsigned int line)
{
    TTokenListItemPtr new = malloc(sizeof(TTokenListItem));
    
    if (new != NULL) // malloc successful
    {
        // initialization of item data
        new->id = token->id;
        new->content = token->content;
        new->next = NULL;
        new->line = line;
        
        // adding new item to given token list
        if (tokenList->first == NULL)
        {
            tokenList->first = tokenList->last = new;
        }
        else
        {
            tokenList->last->next = new;
            tokenList->last = new;
        }
        
        return 0;
        
    }
    else // malloc error
    {
        return ERR_ALLOC;
    }
}

/*
 * Frees all memory used by Token List.
 */
void tokenListDispose (TTokenListPtr tokenList)
{
    TTokenListItemPtr tmp;

    // deletion od items, one by one
    while (tokenList->first != NULL)
    {
        if (tokenList->first->content != NULL)
        {
            free (tokenList->first->content);
        }
        
        tmp = tokenList->first;
        tokenList->first = tokenList->first->next;
        free(tmp);
    }

    // freeing of memory after token list structure
    free(tokenList);
    tokenList = NULL;
}

/* End of token_list.c */
