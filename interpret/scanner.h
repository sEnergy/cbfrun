/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef SCANNER_H_INCLUDED
#define SCANNER_H_INCLUDED

#include "token_type.h"
#include "source_type.h"
#include "errors.h"

#define BINARY_DIGITS sizeof(unsigned int)*8;
#define DEFAULT_STRING_LENGTH 8

/*
 * Implementation of Scanner - Lexical analyzer. On each call, Scanner reads
 * one token from given source file. Also, Scanner updates info about posiniton
 * within source file, stored as row&columb value in TSource structure.
 * 
 * If everything is OK, token ID and token content (if any) is stored in
 * token structure passed as argument and zero value is returned.
 * 
 * On error, both token ID and content are zero/NULL, lex error position is
 * stored in errDetail structure and nonzero error code is returned.
 */
int readNextToken (TSourcePtr src, TTokenPtr token, TErrorDetailPtr errDetail);

/*
 * Function called when quotation mark is found - means there is string to read.
 * String consits of printable characters and/or \n \" escape sequences.
 * 
 * If everything is OK, token ID is set to T_STRING and token content pointer is
 * set to point to allocated memory with string and zero value is returned.
 * 
 * On error, both token ID and content are undefined, lex error position is
 * stored in errDetail structure and nonzero error code is returned. 
 */
int scanString (TSourcePtr src, TTokenPtr token, TErrorDetailPtr errDetail);

/*
 * Function called when apostrophe and ower-case letter D are read, which means
 * start of decinal integer.
 * 
 * If everything is OK, token ID is set to T_INT and token content pointer is
 * set to point to allocated memory with letter d and string of scanned digits.
 * 
 * On error, both token ID and content are undefined, lex error position is
 * stored in errDetail structure and nonzero error code is returned. 
 */
int scanDecInteger (TSourcePtr src, TTokenPtr token, TErrorDetailPtr errDetail);

/*
 * Function called when apostrophe and ower-case letter B are read, which means
 * start of binary integer.
 * 
 * If everything is OK, token ID is set to T_INT and token content pointer is
 * set to point to allocated memory with letter d and string of scanned digits.
 * 
 * On error, both token ID and content are undefined, lex error position is
 * stored in errDetail structure and nonzero error code is returned. 
 */
int scanBinInteger (TSourcePtr src, TTokenPtr token, TErrorDetailPtr errDetail);

#endif

/* End of file scanner.h */
