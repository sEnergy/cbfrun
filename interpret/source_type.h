/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef SOURCE_TYPE_H_INCLUDED
#define SOURCE_TYPE_H_INCLUDED

#include <stdio.h>

/*
 * Structure that carries info about opened source file - pointer to stream
 * and position within file specified by rows and columbs.
 */
typedef struct TSource {
    FILE* source;
    unsigned long int r, c;
} TSource;

typedef TSource* TSourcePtr;

#endif

/* End of file source_type.h */
