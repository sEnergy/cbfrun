/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef LL_TABLE_H_INCLUDED
#define LL_TABLE_H_INCLUDED

#define NTERM_NUMBER 40
#define TERM_NUMBER 12

/*
 * Type for LL(1) parsing table for language lecRAM - 40 nonterminals and
 * 12 terminals.
 */
typedef char TLLTable [NTERM_NUMBER][TERM_NUMBER];

/*
 * Function that fills array of type TLLTable by data of LL(1) parsing table
 * of language lecRAM.
 */
void llTableInit (TLLTable dest);

#endif

/* End of file ll_table.h */
