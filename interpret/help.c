/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>

/*
 * Printing of help text.
 */
void helpPrint (void)
{
    printf( "\n-------------------------------------------------\n"
            " cbfrun - the interpreter for Cleverer Brainfuck \n"
            "-------------------------------------------------\n"
            " To intepret .cbf file with this intepreter, run\n"
            " it like this:\n\n"
            "             $ ./cbfrun <filename>\n\n"
            " If you need more information about intepreter\n"
            " or language CBF  itself, see documentation you \n"
            " got with this interpreter.\n\n"
            " If you find a bug, please send e-mail to:\n\n"
            "              mail@marcel-fiala.eu\n\n"
            " If you write, even really short, program in this\n"
            " language, also send it to e-mail address above.\n"
            " It will be included in next distribution of this\n"
            " language.\n\n"
            " Intepreter arguments:\n"
            "    -pedantic          treats warnings as errors\n"
            "    -suppress-warnings           ignore warnings\n"
            "    -debug                     run in debug mode\n"
            "    -h or --help                 print this text\n\n"
            "-------------------------------------------------\n"
            "          (c) Marcel Fiala, 2013-2015\n"
            "-------------------------------------------------\n\n");
}

/* End of file help.c */
