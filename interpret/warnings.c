/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "warnings.h"
#include "terminals.h"

/*
 * Prints warning message on stderr based on given warning code
 */
void warPrint (const int code, const char *sourceFileName, unsigned int line, const unsigned char style)
{
    char *warning, *info;

    switch (code)
    {
        case WARNING_EMPTYSTACK_JUMP:
            warning = "Tried to jump with empty stack.";
            info = "Address unchanged.";
            break;

        case WARNING_EMPTYSTACK_GETTOP:
            warning = "Tried to obtain top value from empty stack.";
            info = "Value in current cell unchanged.";
            break;

        case WARNING_EMPTYSTACK_USETOP:
            warning = "Tried to use top value from empty stack for math operation.";
            info = "Math operation skipped, value in current cell unchanged.";
            break;

        case WARNING_EMPTYSTACK_REMOVE:
            warning = "Tried to remove top value from empty stack.";
            info = "Nothing happened.";
            break;

        case WARNING_EMPTYSTACK_EDIT:
            warning = "Tried to edit top value on empty stack.";
            info = "Nothing happened.";
            break;

        case WARNING_SORT_FEW:
            warning = "Too few items on stack to use sorting function.";
            info = "Sort needs at least 2 items on stack.";
            break;

        case WARNING_SORT_UNITIALIZED:
            warning = "Sorted part of memory contains so far unused cells.";
            info = "Make sure this is correct.";
            break;

        default:
            warning = "Undefined warning.";
            break;
    }

    if (style)
    {
        fprintf(stderr, "\nWarning @ %s (line %u):\n%s\n%s\n\n", sourceFileName, line, warning, info);
    }
    else
    {
        fprintf(stderr, "\nError %d @ %s (line %u):\n%s\nIf you need help, run program with parameter [-h|--help].\n\n", 
            code+PEDANTIC_WARNING_CODE_DIFF, sourceFileName, line, warning);
    }
}

/* End of file errors.c */
