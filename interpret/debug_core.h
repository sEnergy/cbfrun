/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#define LEN 50

#define STATE_START 1
#define STATE_SET 2
#define STATE_VALUE 3
#define STATE_TO 4
#define STATE_CELLNUMBER 5

#define TARGET_ADDRESS 1
#define TARGET_STACK 2
#define TARGET_CELL 3
#define TARGET_PUSH 4

#define TERMINATE 1
#define CONTINUE 0

#include "stack.h"
#include "memory.h"

#define MsgBadCommand() fprintf(stderr, "\n>> error: unknown command, try again...\n\n")

#define MsgBadAddress() fprintf(stderr, "\n>> error: bad cell address, try again...\n\n")

#define MsgHighAddress() fprintf(stderr, "\n>> error: cannot use address of unitialized cell, try again...\n\n")

#define MsgBadValue() fprintf(stderr, "\n>> error: bad value, try again...\n\n")

#define MsgEmptyStack() fprintf(stderr, "\n>> error: stack is empty, try again...\n\n")


/*
 * Core ofdebugger - accepts user's commands and calls debug display functions.
 */
int debugCore (const TMemoryPtr m, const TStackPtr stack, unsigned int line);

/*
 * Prints message at start of debugger.
 */
void debugStartMessage (char* sourceFileName);

/*
 * Prints message at termination of debugger.
 */
void debugEndMessage (void);

/*
 * Prints message debug help.
 */
void debugHelp(void);

/* End of file debug_core.h */
