/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

#include "ic_type.h"

/*
 * Direct access to top integer value
 */
#define StackTopInt(stack) *((unsigned int*)stack->top->content)

/*
 * Increments integer value on the top of stack.
 *
 * Warning: Causes SEGMENTATION FAULT when used on empty stack.
 */
#define StackTopInc(stack) ++(*((unsigned int*)stack->top->content))

/*
 * Decrements integer value on the top of stack.
 *
 * Warning: Causes SEGMENTATION FAULT when used on empty stack.
 */
#define StackTopDec(stack) --(*((unsigned int*)stack->top->content))

/*
 * True stack is empty, false otherwise.
 */
#define StackEmpty(stack) !(stack->size)

/*
 * Single item of stack. Points to item "below".
 */
typedef struct TStackItem {
    void* content;
    struct TStackItem* next;
} TStackItem;

/*
 * Stack implemented as linear list with access to first (top) item only.
 */
typedef struct TStack {
    struct TStackItem* top;
    unsigned long long int size;
} TStack;

/*
 * Definiton of pointers to Stack and StackItem
 */
typedef TStack* TStackPtr;
typedef TStackItem* TStackItemPtr;

/*
 * Initializes new stack and returns its pointer.
 *
 * Returns NULL on failure.
 */
TStackPtr stackInit (void);

/*
 * Pushes new integer item to the top of stack.
 *
 * Returns zero on succes, error code otherwise.
 */
int stackPushInt (TStackPtr stack, const unsigned int value);

/*
 * Returns integer value from top of stack and removes item carrying this value.
 *
 * Warning: Causes SEGMENTATION FAULT when called on empty stack.
 *          May cause error when called on stack of different data type.
 */
unsigned int stackPopInt (TStackPtr stack);

/*
 * Pushes new Instruction Pointer item to the top of stack.
 *
 * Returns zero on succes, error code otherwise.
 */
int stackPushInstPtr (TStackPtr stack, const TInstructionPtr ptr);

/*
 * Returns Instruction Pointer value from top of stack and removes item carrying 
 * this value.
 *
 * Warning: Causes SEGMENTATION FAULT when called on empty stack.
 *          May cause error when called on stack of different data type.
 */
TInstructionPtr stackPopInstPtr (TStackPtr stack);

/*
 * Frees all memory used by stack.
 */
void stackDispose (TStackPtr stack);

/*
 * Frees all memory used by header of Instruction Pointer stack.
 */
void PtrStackDispose (TStackPtr stack);

#endif

/* End of file stack.h */
