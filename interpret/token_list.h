/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef TOKEN_LIST_H_INCLUDED
#define TOKEN_LIST_H_INCLUDED

#include "token_type.h"

/*
 * Single item of Token List. Points to next item only.
 */
typedef struct TTokenListItem {
    unsigned char id;
    unsigned int line;
    char* content;
    struct TTokenListItem* next;
} TTokenListItem;

/*
 * Token list implemented as single-linked linear list.
 */
typedef struct TTokenList {
    struct TTokenListItem *first, *last;
} TTokenList;

typedef TTokenList* TTokenListPtr;
typedef TTokenListItem* TTokenListItemPtr;

/*
 * Initializes new Token List and returns pointer to it.
 *
 * Returns NULL on failure.
 */
TTokenListPtr tokenListInit (void);

/*
 * Adds specified token in the end of Token List passed as first argument.
 * 
 * Returns zero on success, nonzero error code on failure.
 */
int tokenListAdd (TTokenListPtr tokenList, TTokenPtr token, unsigned int line);

/*
 * Frees all memory used by Token List.
 */
void tokenListDispose (TTokenListPtr tokenList);

#endif

/* End of file token_list.h */
