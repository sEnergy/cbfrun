/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef TOKEN_TYPE_H_INCLUDED
#define TOKEN_TYPE_H_INCLUDED

/*
 * Structure that carries basic token info
 */
typedef struct TToken {
    unsigned char id;
    char* content;
} TToken;

typedef TToken* TTokenPtr;

#endif

/* End of file token_type.h */