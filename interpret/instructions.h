/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef INSTRUCTIONS_H_INCLUDED
#define INSTRUCTIONS_H_INCLUDED

/*
 * Definifiton of instructions IDs.
 */
enum instructions
{
    I_VALINC = 1,   // increment value at current address
    I_VALDEC,       // decrement value at current address
    I_SETORDINAL,   // set value at current address to ordinal value of given character     
    I_SETINT,       // set value at current address to given integer value
    I_RESET,        // set address to zero
    I_ADDRINC,      // increment address
    I_ADDRDEC,      // decrement address
    I_SHORTJUMP,    // short jump - change of address euqal or smaller to 10
    I_LONGJUMP,     // jump to address currently at the top of stack
    I_SAVEADDR, //10// push current address to stack
    I_WHILE,        // value == 0 -> skip scope; value != 0 enter scope
    I_WHILE_NOT,    // value != 0 -> skip scope; value == 0 enter scope
    I_IF,           // value == 0 -> skip scope; value != 0 enter scope
    I_IF_NOT,       // value != 0 -> skip scope; value == 0 enter scope
    I_FOR_BEGIN,    // counter == 0 -> skip scope; counter != 0 enter scope and lower counter
    I_FOR_COUNTER,  // sets counter for new for cycle
    I_CYCLE_END,    // return control back to condition
    I_IF_END,       // this just marks end of scope, nothing more
    I_PUTCHAR,      // print value at current address as character
    I_PUTINT,   //20// print value at current address as decimal number
    I_PUTSTRING,    // print string starting at current address
    I_GETCHAR,      // read char from standard input to current address, EOF == 0  
    I_PUSH,         // push value from current address to stack
    I_POP,          // pop top value from stack to current address
    I_TOP,          // copy top value from stack to current address
    I_REMOVE,       // remove top value from stack
    I_ADD,          // add value from top of stack to value at current address
    I_SUBTRACT,     // subtract value from top of stack from value at current address
    I_DEVIDE,       // devide value at current address by value from top of stack
    I_MULTIPLY, //30// multiply value at current address by value from top of stack
    I_EMPTY,        // set value at current address to 1 if stack is empty, to 0 otherwse
    I_STACKINC,     // increment top value on stack
    I_STACKDEC,     // decrement top value on stack
    I_ECHO,         // print given string
    I_SAVE_STRING,  // save given string to current address
    I_DEBUG,        // start debug interface
    I_END,          // terminate interpreted program
    I_PROC_START,   // just marks beginning of procedure scope
    I_PROC_CALL,    // attempt to all given procedure
    I_PROC_END, //40// jump back to calling instruction
    I_PROCSCOPE_END,// just marker of procedure definitons scope end
    I_SORT          // sort memory between two top adresses on stack to ascending order
};

#endif

/* End of file instructions.h */
