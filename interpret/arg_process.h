/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef ARGUMENT_PROCESS_H_INCLUDED
#define ARGUMENT_PROCESS_H_INCLUDED

#include "arg_type.h"

#define LEGAL_ARGC_MIN 2
#define LEGAL_ARGC_MAX 4

// length of source file extension (.cbf)
#define EXTENSION_LENGTH 4

// codes to return after argument check
#define CODE_INTERPRETER -1
#define CODE_HELP -2

/*
 * Processes interpreter's arguments and returns code accordingly (either
 * INTERPRETATION/HELP code or some ERR_ code).
 */
int argumentProcess (const int argc, char *argv[], TArgumentsPtr args);

#endif

/* End of file arg_process.h */
