/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "core.h"
#include "parser.h"
#include "interpreter.h"
#include "token_list.h"
#include "ic_generator.h"
#include "ic_type.h"

int interpretationCore (TArgumentsPtr args, TErrorDetailPtr errorDetail)
{
    int code;
    
    TTokenListPtr tokenList;
    TInnerCodePtr ic;
    TInstructionPtr* procedures;
    
    // initialization of token list 
    if ((tokenList = tokenListInit()) == NULL)
    {
        return ERR_ALLOC;
    }
    
    // load of source code & top-down syntax analysis
    if ((code = parserCore(args, tokenList, errorDetail)) != 0)
    {
        tokenListDispose(tokenList);
        return code;
    }
    
    // initilization of inner code structure
    if ((ic = innerCodeInit()) == NULL)
    {
        tokenListDispose(tokenList);
        return code;
    }
    
    /* 
     * Initialization of array of defined procedures - pointers to first 
     * instruction of procedure, if defined. Otherwise, value stays NULL
     */
    if ((procedures = calloc(PROCEDURE_NUMBER, sizeof(TInstructionPtr))) == NULL)
    {
        innerCodeDispose(ic);
        tokenListDispose(tokenList);
        
        return code;
    }
    
    code = innerCodeGenerator(tokenList, ic, procedures, errorDetail);
    tokenListDispose(tokenList);
    
    if (code)
    {
        free(procedures);
        innerCodeDispose(ic);
        
        return code;
    }
    
    code = interpreter(ic, procedures, errorDetail, args);
    
    innerCodeDispose(ic);
    free(procedures);
    
    return code;
}

/* End of core.c */
