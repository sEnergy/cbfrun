/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/
 
#include <string.h>

#include "ll_table.h"
#include "terminals.h"

/*
 * Function that fills array of type TLLTable by data of LL(1) parsing table
 * of language lecRAM.
 */
void llTableInit (TLLTable dest)
{
    /*
     * LL(1) parsing table for lecRAM. 
     * 
     * rows = terminals (also defined in terminals.h)
     * columbs = nornterminals (fully defined in nonterminals.h)
     * 
     * 0 == error (no usable rule)
     * 1..48 == rule to use
     */
    TLLTable src =
        
// PROG PRDEFSC PRDEFL PRDEF STLIST ST ADDCH ADDCHARG MODCON  MODUL MODST INST
  {{ 1,    2,     0,     0,    6,   0,   0,     14,      0,     0,    0,    0 }, //T_EOF
  {  1,    2,     0,     0,    7,  11,   0,     14,     16,    18,    0,    0 }, //T_MODULATOR
  {  1,    2,     0,     0,    7,  10,   0,     14,      0,     0,    0,    0 }, //T_SHARP
  {  0,    0,     0,     0,    0,   0,   0,      0,      0,     0,    0,    0 }, //T_CURLYLEFT
  {  0,    0,     0,     0,    6,   0,   0,     14,      0,     0,    0,    0 }, //T_CURLYRIGHT
  {  1,    2,     0,     0,    7,  11,   0,     14,     16,    17,   21,    0 }, //T_BRACKETLEFT
  {  0,    0,     0,     0,    6,   0,   0,     14,      0,     0,    0,    0 }, //T_BRACKETRIGHT
  {  1,    2,     0,     0,    7,  11,   0,     14,     16,    17,   20,    0 }, //T_PARENLEFT
  {  0,    0,     0,     0,    6,   0,   0,     14,      0,     0,    0,    0 }, //T_PARENRIGHT
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   43 }, //T_INT
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   42 }, //T_CHAR
  {  0,    0,     0,     0,    0,   0,   0,     15,      0,     0,    0,    0 }, //T_DIGIT
  {  1,    2,     4,     5,    7,   8,   0,     14,      0,     0,    0,   41 }, //T_ID
  {  1,    2,     0,     0,    7,  11,   0,     14,     16,    17,   19,    0 }, //T_STRING
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   22 }, //T_DEBUG
  {  1,    3,    48,     0,    0,   0,   0,      0,      0,     0,    0,    0 }, //T_DOLLAR
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   23 }, //T_GETCHAR
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   24 }, //T_PUTCHAR
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   25 }, //T_PUTINT
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   26 }, //T_PUTSTRING
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   27 }, //T_PUSH
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   28 }, //T_POP
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   29 }, //T_TOP
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   30 }, //T_REMOVE
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   31 }, //T_SAVEADDR
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   32 }, //T_LONGJUMP
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   33 }, //T_ADD
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   34 }, //T_SUBTRACT
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   36 }, //T_MULTIPLY
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   35 }, //T_DEVIDE
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   39 }, //T_EMPTY
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   37 }, //T_TOPINC
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   38 }, //T_TOPDEC
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   45 }, //T_VALINC
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   46 }, //T_VALDEC
  {  1,    2,     0,     0,    7,   9,  12,     14,      0,     0,    0,    0 }, //T_ADDRINC
  {  1,    2,     0,     0,    7,   9,  13,     14,      0,     0,    0,    0 }, //T_ADDRDEC
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   40 }, //T_RESET
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   47 }, //T_SORT
  {  1,    2,     0,     0,    7,   8,   0,     14,      0,     0,    0,   44}}; //T_RETURN
   
  /* 
   * Table was initialized as local variable, so memcpy is needed to copy table
   * data to array passed as argument of function.
   */
  memcpy(dest, src, NTERM_NUMBER*TERM_NUMBER);
}

/* End of ll_table.c */
