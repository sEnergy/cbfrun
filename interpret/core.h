/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef CORE_H_INCLUDED
#define CORE_H_INCLUDED

#include "arg_type.h"
#include "errors.h"

int interpretationCore (TArgumentsPtr args, TErrorDetailPtr errorDetail);

#endif

/* End of file core.h */
