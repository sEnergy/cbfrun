/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>

#include "ic_generator.h"
#include "ic_type.h"
#include "terminals.h"
#include "instructions.h"
#include "intval.h"
#include "stack.h"

/*
 * Function that processes Token List and based on gained data, generates
 * Inner Code which is directly used by Interpreter module. Also, this function
 * saves pointers to defined procedures in special array.
 * 
 * Returns zero value on success or nonzero values otherwise. In addition, if
 * error occurs, as much as possible info is saved into ErrorDetal structure.
 */
int innerCodeGenerator (TTokenListPtr tokenList, TInnerCodePtr ic, 
                        TInstructionPtr* procedures, TErrorDetailPtr err)
{
    int code = 0;
    
    /* 
     * Info about if the current token is inside procedure definitions scope
     * and/or procedure definiton.
     */
    bool procDefScope = true;
    bool procDef = false;
    
    /*
     * Stacks used to save opened cycle & if constructions, so when end 
     * of these constructions are found, it is possible to link them with
     * pointers that allow interpreter to move within inner code very fast.
     */
    TStackPtr ifStack;
    TStackPtr cycleStack;
    
    if ((ifStack = stackInit()) == NULL)
    {
        return ERR_ALLOC;
    }
    
    if ((cycleStack = stackInit()) == NULL)
    {
        stackDispose(ifStack);
        return ERR_ALLOC;
    }
    
    // variable working as parameter for addInstruction function    
    TInstruction inst = { .id = 0, .line = 0, .arg = NULL };
    
    // currently processed token
    TTokenListItemPtr token = tokenList->first;
    
    if (token->id != T_DOLLAR) // empty procedures definiton scope
    {
        procDefScope = false;
    }
    else // start of procedures definition scope
    {
        token = token->next;
    }
    
    /*
     *  Processing of all tokens in token list, one by one.
     */
    while (token != NULL)
    {
        inst.line = token->line;
        inst.arg = NULL;
        
        switch (token->id)
        {
            case T_DOLLAR:
                procDefScope = false;
                inst.id = I_PROCSCOPE_END;
                break;
            
            case T_EOF:     // Return (end) instruction is placed when EOF
            case T_RETURN:  // or T_RETURN token is found.
                inst.id = I_END;
                break;
                
            case T_DEBUG:
                inst.id = I_DEBUG;
                break;
                
            case T_TOPINC:
                inst.id = I_STACKINC;
                break;
                
            case T_TOPDEC:
                inst.id = I_STACKDEC;
                break;
                
            case T_EMPTY:
                inst.id = I_EMPTY;
                break;
                
            case T_PUSH:
                inst.id = I_PUSH;
                break;
                
            case T_POP:
                inst.id = I_POP;
                break;
                
            case T_TOP:
                inst.id = I_TOP;
                break;
                
            case T_REMOVE:
                inst.id = I_REMOVE;
                break;
                
            case T_ADD:
                inst.id = I_ADD;
                break;
                
            case T_SUBTRACT:
                inst.id = I_SUBTRACT;
                break;
                
            case T_DEVIDE:
                inst.id = I_DEVIDE;
                break;
                
            case T_MULTIPLY:
                inst.id = I_MULTIPLY;
                break;
                
            case T_PUTCHAR:
                inst.id = I_PUTCHAR;
                break;
                
            case T_PUTINT:
                inst.id = I_PUTINT;
                break;
                
            case T_PUTSTRING:
                inst.id = I_PUTSTRING;
                break;
                
            case T_GETCHAR:
                inst.id = I_GETCHAR;
                break;
                
            case T_CURLYLEFT:
                procDef = true;
                token = token->next;
                continue;
                break;
                
            case T_CURLYRIGHT:
                inst.id = I_PROC_END;
                procDef = false;
                break;
                
            case T_VALINC:
                inst.id = I_VALINC;
                break;
                
            case T_VALDEC:
                inst.id = I_VALDEC;
                break;
                
            case T_CHAR: { // set ordinal value instruction
                
                char* tmp = malloc(sizeof(char));
                
                if (tmp == NULL)
                {
                    code = ERR_ALLOC;
                    break;
                }
                
                tmp[0] = token->content[0];
                
                inst.id = I_SETORDINAL;
                inst.arg = tmp;
                
                } break;
                
            case T_RESET:
                inst.id = I_RESET;
                break;
                
            case T_ADDRINC: { // increment of address
            
                if (token->next->id == T_DIGIT) // short jump
                {
                    token = token->next;
                    
                    char* tmp = malloc(sizeof(char));
                    
                    if (tmp == NULL)
                    {
                        code = ERR_ALLOC;
                        break;
                    }
                
                    tmp[0] = (token->content[0])-'0';
                    
                    // zero stands for ten
                    if (tmp[0] == 0)
                    {
                        tmp[0] = 10;
                    }
                    
                    free(token->content);
                    token->content = NULL;
                    
                    inst.id = I_SHORTJUMP;
                    inst.arg = tmp;
                    inst.line = token->line;
                }
                else // simple increment of address by one
                {
                   inst.id = I_ADDRINC; 
                }
                
                } break;
                
            case T_ADDRDEC: { // decrement of address
            
                if (token->next->id == T_DIGIT) // short jump
                {
                    token = token->next;
                    
                    char* tmp = malloc(sizeof(char));
                    
                    if (tmp == NULL)
                    {
                        code = ERR_ALLOC;
                        break;
                    }
                
                    tmp[0] = -((token->content[0])-'0');
                    
                    // zero stands for ten
                    if (tmp[0] == 0)
                    {
                        tmp[0] = -10;
                    }
                    
                    free(token->content);
                    token->content = NULL;
                    
                    inst.id = I_SHORTJUMP;
                    inst.arg = tmp;
                    inst.line = token->line;
                }
                else // simple decrement of address by one
                {
                   inst.id = I_ADDRDEC; 
                }
                
                } break;
                
            case T_LONGJUMP:
                inst.id = I_LONGJUMP;
                break;
                
            case T_SAVEADDR:
                inst.id = I_SAVEADDR;
                break;
                
            case T_STRING: {
                
                inst.id = I_ECHO;
                inst.arg = token->content;
                token->content = NULL;
                
                } break;
                
            case T_INT: { // set integer value value
            
                inst.id = I_SETINT;
                unsigned int* tmp = malloc(sizeof(unsigned int));
                
                if (tmp == NULL)
                {
                    code = ERR_ALLOC;
                    break;
                }
                
                code = intval(token->content, tmp);
                
                if (code)
                {
                    err->data = token->content;
                    token->content = NULL;
                    free(tmp);
                }
                else
                {
                    inst.arg = tmp;
                }
                
                } break;
                
            case T_MODULATOR: { // start of modulable statement
                
                token = token->next;
                inst.line = token->line;
                
                char tmp = token->id;
                
                if (tmp == T_STRING) // save string
                {
                    inst.id = I_SAVE_STRING;
                    inst.arg = token->content;
                    token->content = NULL;
                }
                else if (tmp == T_PARENLEFT) // !if
                {
                    inst.id = I_IF_NOT;
                }
                else if (tmp == T_BRACKETLEFT) // !while
                {
                    inst.id = I_WHILE_NOT;
                }
                
                } break;
                
            case T_PARENLEFT:
                inst.id = I_IF;
                break;
                
            case T_PARENRIGHT:
                inst.id = I_IF_END;
                break;
                
            case T_BRACKETLEFT:
                inst.id = I_WHILE;
                break;
                
            case T_BRACKETRIGHT:
                inst.id = I_CYCLE_END;
                break;
                
            case T_SHARP: { // start of for construction
                
                token = token->next; // jump to counter
                inst.line = token->line;
                
                unsigned int* tmp = malloc(sizeof(unsigned int));
                
                if (tmp == NULL)
                {
                    code = ERR_ALLOC;
                    break;
                }
                
                if ((code = intval(token->content, tmp)) != 0)
                {
                    err->data = token->content;
                    free(tmp);
                }
                
                inst.id = I_FOR_COUNTER;
                inst.arg = tmp;
        
                // adding counter instruction to Inner Code
                if ((code = addInstruction(ic, &inst)) != 0)
                {
                    break;
                }
                
                token = token->next;
                inst.line = token->line;
                inst.id = I_FOR_BEGIN;
                
                } break;
                
            case T_ID: { // start of procedure definiton of just a call
                
                if (procDefScope)
                {
                    if (procDef) // call of procedure inside procedure
                    {
                        unsigned char* tmp = malloc(sizeof(unsigned char));
                        
                        if (tmp == NULL)
                        {
                            code = ERR_ALLOC;
                            break;
                        }
                
                        *tmp = (token->content[0])-'a';
                        
                        inst.id = I_PROC_CALL;
                        inst.arg = tmp;    
                    }
                    else // definition of procedure
                    {
                        // procedure not defined yet
                        if (procedures[token->content[0]-'a'] == NULL)
                        {   
                            inst.id = I_PROC_START;
                        }
                        else // procedure already defined
                        {
                            code = ERR_PROC_REDEF;
                            
                            /*
                             * Collecting info about error - line, id of procedure
                             * and ine of previous definition.
                             */
                            TInstructionPtr tmp = procedures[token->content[0]-'a'];
                            err->data = malloc(sizeof(char));
                            
                            err->data[0] = token->content[0];
                            err->line = token->line;
                            err->tokenID = tmp->line;
                        } 
                    }
                }
                else // standard call of procedure
                {
                    unsigned char* tmp = malloc(sizeof(unsigned char));
                    
                    if (tmp == NULL)
                    {
                        code = ERR_ALLOC;
                        break;
                    }
                    
                    *tmp = (token->content[0])-'a';
                    
                    inst.id = I_PROC_CALL;
                    inst.arg = tmp;
                }
                
                } break;
                
            case T_SORT:
                inst.id = I_SORT;
                break;
                
            default:
                break;
        }
        
        if (code) // error
        {
            err->line = token->line;
            err->columb = UINT_MAX;
            break;
        }

        // adding instruction to Inner Code
        if ((code = addInstruction(ic, &inst)) != 0)
        {
            break;
        }
        
        unsigned char tmp = inst.id;
        
        /*
         * Linking some instructions with pointers to allow interpreter move
         * within source code as fast as possible.
         */
        if (tmp == I_IF || tmp == I_IF_NOT)
        {
            if ((code = stackPushInstPtr(ifStack, ic->last)) != 0)
            {
                break;
            }
        }
        else if (tmp == I_WHILE || tmp == I_WHILE_NOT || tmp == I_FOR_BEGIN)
        {
            if ((code = stackPushInstPtr(cycleStack, ic->last)) != 0)
            {
                break;
            }
        }
        else if (tmp == I_IF_END)
        {
            TInstructionPtr tmp = stackPopInstPtr(ifStack);
            tmp->arg = ic->last;
        }
        else if (tmp == I_CYCLE_END)
        {
            TInstructionPtr tmp = stackPopInstPtr(cycleStack);
            tmp->arg = ic->last;
            ic->last->arg = tmp; // link from cycle end to cycle beginning
        }
        else if (tmp == I_PROC_START)
        {
            procedures[token->content[0]-'a'] = ic->last;
        }
        else if (tmp == I_PROCSCOPE_END)
        {
            ic->bodyStart = ic->last;
        }
        
        token = token->next;
    } 
    
    PtrStackDispose(ifStack);
    PtrStackDispose(cycleStack);
    
    return code;
}

/* End of ic_generator.c */
