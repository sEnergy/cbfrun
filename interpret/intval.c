/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdlib.h>

#include "intval.h"
#include "errors.h"

/*
 * Converts valid integer representation in string form to unsigned int.
 * 
 * Returns error code, if the value is above UINT_MAX. Zero otherwise.
 * 
 * May cause SEGMENTATION FAULT if wrong arguments are passed to this function.
 */
int intval (char* string, unsigned int* dest)
{
    unsigned int result;
    unsigned long tester;
    
    if (string[0] == 'd') // decimal number
    {
        tester = strtol(string+1, NULL, 10);
    }
    else // binary number
    {
        tester = strtol(string+1, NULL, 2);
    }
    
    /*
     * Result of conversion is long. If this values does not fit in unsigned
     * int, it is considered error.
     */
    result = tester;
    
    if (result != tester) // errror - number too big
    {
        return ERR_BIG_NUMBER;
    }
    else // everything ok
    {
        *dest = result;
        return 0;
    }
}

/* End of intval.c */
