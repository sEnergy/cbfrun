/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef RULES_H_INCLUDED
#define RULES_H_INCLUDED

#define RULES_NUMBER 48
#define MAX_RULE_LENGTH 6

/*
 * Type for rules of language lecRAM - 48 rules of max length 6
 */
typedef char TRules [RULES_NUMBER][MAX_RULE_LENGTH];

/*
 * Function that initializes array of rules for parser.
 */
void rulesInit (TRules dest);

#endif

/* End of file rules.h */
