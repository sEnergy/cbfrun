/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef INTVAL_H_INCLUDED
#define INTVAL_H_INCLUDED

/*
 * Converts valid integer representation in string form to unsigned int.
 * 
 * Returns error code, if the value is above UINT_MAX. Zero otherwise.
 * 
 * May cause SEGMENTATION FAULT if wrong arguments are passed to this function.
 */
int intval (char* string, unsigned int* dest);

#endif

/* End of file intval.h */
