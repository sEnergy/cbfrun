/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef IC_GENERATOR_H_INCLUDED
#define IC_GENERATOR_H_INCLUDED

#include "token_list.h"
#include "ic_type.h"
#include "errors.h"

#define PROCEDURE_NUMBER 26

/*
 * Function that processes Token List and based on gained data, generates
 * Inner Code which is directly used by Interpreter module. Also, this function
 * saves pointers to defined procedures in special array.
 * 
 * Returns zero value on success or nonzero values otherwise. In addition, if
 * error occurs, as much as possible info is saved into ErrorDetal structure.
 */
int innerCodeGenerator (TTokenListPtr tokenList, TInnerCodePtr ic, 
                        TInstructionPtr* procedures, TErrorDetailPtr err);

#endif

/* End of file ic_generator.h */
