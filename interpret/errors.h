/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef ERRORS_H_INCLUDED
#define ERRORS_H_INCLUDED

/*
 * Structure that carries detailed info about error that occured.
 */
typedef struct TErrorDetail {
    unsigned int line, columb;
    unsigned int tokenID;
    char* data;
} TErrorDetail;

typedef TErrorDetail* TErrorDetailPtr;

/*
 * Error codes to clearly identify errors and give user proper info.
 */
enum errorCodes
{
    ERR_ARGCOUNT = 1,
    ERR_ARG,
    ERR_ARG_REUSE,
    ERR_ARG_MIX,
    ERR_HELP_NON_STANDALONE,
    ERR_EXTENSION,
    ERR_NOFILE,
    ERR_ALLOC,
    ERR_LEXICAL,
    ERR_LEX_INVALID,
    ERR_LEX_STRING,
    ERR_LEX_DIGIT,
    ERR_BIG_NUMBER,
    ERR_UNEXPECTED_TOKEN,
    ERR_PROC_REDEF,
    ERR_PROC_UNDEF,
    ERR_DEVIDE_BY_ZERO
};

/*
 * Prints error message on stderr based on given error code
 */
void errPrint (const int code, const char *sourceFileName, TErrorDetailPtr err);

#endif

/* End of file errors.h */
