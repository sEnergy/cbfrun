/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "scanner.h"
#include "token_list.h"
#include "rules.h"
#include "terminals.h"
#include "nonterminals.h"
#include "ll_table.h"
#include "stack.h"
#include "source_type.h"
#include "errors.h"

/*
 * Syntax analyzer - parser implementing predictive algorith using LL parsing 
 * table, expansion rules and stack.
 * 
 * When source file whose name is saved in structure args, all loaded tokens
 * are saved to passed tokenList and zero value is returned. Otherwise
 * nonzero value (error code) is returned and info about error that occured
 * is save to structure arrorDetail.
 */
int parserCore (TArgumentsPtr args, TTokenListPtr tokenList, TErrorDetailPtr errorDetail)
{
    int code;
    
    TSource src; // opened source file & info about it
    TToken token; // current token
    TStackPtr stack; // stack for use by predictive algorithm of SA
    TLLTable llTable; // LL(1) parsing table
    TRules rules; // expansion rules
    
    // opening of source file
    if ((src.source = fopen(args->sourceFileName, "r")) == NULL)
    {
        return ERR_NOFILE;
    }
    else
    {
        src.r = 1;
        src.c = 0;
    }
    
    // initializing stack
    if ((stack = stackInit()) == NULL)
    {
        fclose(src.source);
        return ERR_ALLOC;
    }
    else
    {
        // pushing starting nonterminal NT_PROGRAM to stack
        if ((code = stackPushInt(stack, NT_PROGRAM)) != 0)
        {
            fclose(src.source);
            stackDispose(stack);
            return ERR_ALLOC;
        }
    }
    
    llTableInit(llTable); // initialization of LL(1) parsing table
    rulesInit(rules); // initialization of rules
    
    // read of first token
    if ((code = readNextToken(&src, &token, errorDetail)) != 0)
    {
        fclose(src.source);
        stackDispose(stack);
        return ERR_ALLOC;
    }
    
    // save loaded token
    if ((code = tokenListAdd(tokenList, &token, src.r)) != 0)
    {
        fclose(src.source);
        stackDispose(stack);
        return ERR_ALLOC;
    }
    
    /*
     * Top-down syntax analysis implementing predictive algorith using
     * LL(1) parsing table, rules and stack.
     */
    do
    {
        // all tokens read (EOF loaded) & stack empty -> SA success
        if (StackEmpty(stack) && token.id == T_EOF)
        {
            code = -1;
            continue;
        }
        
        if (StackTopInt(stack) < NT_DEFAULT) // terminal is on top of stack
        {
            if (StackTopInt(stack) == token.id) // stack top & current token match
            {
                stackPopInt(stack);
                
                // possible SA success
                if (StackEmpty(stack) && token.id == T_EOF)
                {
                    code = -1;
                    continue;
                }
                
                // read next token
                if ((code = readNextToken(&src, &token, errorDetail)) != 0)
                {
                    continue;
                }
                
                // save loaded token
                code = tokenListAdd(tokenList, &token, src.r);
        
                continue;
            }
            else
            {
                code = ERR_UNEXPECTED_TOKEN;
                continue;
            }
        }
        else // nonterminal on top of stack
        {
            // parse expansion rule
            int ruleNumber = llTable[(token.id)-1][(StackTopInt(stack))-NT_DEFAULT];
            
            if (ruleNumber) // rule found
            {
                stackPopInt(stack);
                
                char* rule = rules[ruleNumber-1];
                
                /* 
                 * Expansion of nonterminal at the top of stack according
                 * to parsed rule.
                 */
                for (int i = 0; rule[i] != 0; ++i)
                {
                    if ((code = stackPushInt(stack, rule[i])) != 0)
                    {
                        return ERR_ALLOC;
                    }
                }
            }
            else // rule not found -> error
            {
                code = ERR_UNEXPECTED_TOKEN;
                continue;
            }
        }
        
    } while (code == 0);
    
    fclose(src.source);
    stackDispose(stack);
    
    if (code != -1) // if error occured
    {
        // saving info about position of error
        errorDetail->line = src.r;
        errorDetail->columb = src.c;
        
        // saving info about token that caused error
        if (token.content == NULL) // simple token without token content
        {
            errorDetail->tokenID = token.id;
        }
        else // token with token content
        {
            int tmp = strlen(token.content);
            
            errorDetail->tokenID = token.id;
            errorDetail->data = calloc(tmp+1, sizeof(char));
            
            if (errorDetail->data == NULL)
            {
                return ERR_ALLOC;
            }
            else
            {
                memcpy(errorDetail->data, token.content, tmp);
            }
        }
    }
    
    return (code == -1)? 0:code; // -1 == succes, other values == error
}

/* End of parser.c */
