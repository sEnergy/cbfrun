/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "interpreter.h"
#include "errors.h"
#include "warnings.h"
#include "ic_type.h"
#include "instructions.h"
#include "stack.h"
#include "memory.h"
#include "quicksort.h"
#include "debug_core.h"

int interpreter (TInnerCodePtr ic, TInstructionPtr* procedures, TErrorDetailPtr err, TArgumentsPtr args)
{
    int code = 0;
    bool instructionShift = true;
    
    TMemory m;
    TStackPtr procCalls, forCycles, stack;
    TInstructionPtr instruction;
    
    if (ic->bodyStart == NULL)
    {
        instruction = ic->first;
    }
    else
    {
        instruction = ic->bodyStart->next;  
    }
    
    if ((procCalls = stackInit()) == NULL)
    {
        return ERR_ALLOC;
    }
    
    if ((forCycles = stackInit()) == NULL)
    {
        stackDispose(procCalls);
        return ERR_ALLOC;
    }
    
    if ((stack = stackInit()) == NULL)
    {
        stackDispose(procCalls);
        stackDispose(forCycles);
        
        return ERR_ALLOC;
    }
    
    if (memoryInit(&m))
    {
        stackDispose(procCalls);
        stackDispose(forCycles);
        stackDispose(stack);
        
        return ERR_ALLOC;
    }

    if (args->debug)
	{
		debugStartMessage(args->sourceFileName);
	}
    
    while (instruction != NULL)
    {
        switch (instruction->id)
        {
            // increment value at current address
            case I_VALINC:
                ++(m.data[m.ptr]);
                break;
            
            // decrement value at current address
            case I_VALDEC:
                --(m.data[m.ptr]);
                break;
            
            // set value at current address to ordinal value of given character
            case I_SETORDINAL:
            
                m.data[m.ptr] = *((char*)instruction->arg);
                break;
                
            // set value at current address to given integer value
            case I_SETINT:
                m.data[m.ptr] = *((unsigned int*)instruction->arg);
                break;
            
            // set address to zero
            case I_RESET:
                m.ptr = 0;
                break;
            
            // increment address
            case I_ADDRINC:
                code = incPtr(&m, 1);
                break;
            
            // decrement address
            case I_ADDRDEC:
                decPtr(&m, 1);
                break;
                
            // short jump - change of address euqal or smaller to 10
            case I_SHORTJUMP: {
            
                char tmp = *((char*)instruction->arg);
                
                if (tmp > 0) // jump to higher address
                {
                    code = incPtr(&m, tmp);
                }
                else // jump to lower address
                {
                    decPtr(&m, -tmp);
                }
                
                } break;
            
            // jump to address currently at the top of stack
            case I_LONGJUMP: {
                
                if (!StackEmpty(stack))
                {
                    unsigned int tmp = StackTopInt(stack);

                    if (tmp <= m.highest)
                    {
                        m.ptr = tmp;
                    }
                    else
                    {
                        if ((code = incPtr(&m, tmp-m.highest)) != 0)
                        {
                            break;
                        }
                    }
                    
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_JUMP, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_JUMP;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_JUMP, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // push current address to stack
            case I_SAVEADDR:
                code = stackPushInt(stack, m.ptr);
                break;
            
            // sets counter for new for cycle
            case I_FOR_COUNTER:
                code = stackPushInt(forCycles, *((unsigned int*)instruction->arg));
                break;
                
            // counter == 0 -> skip scope; counter != 0 enter scope & decrement counter
            case I_FOR_BEGIN: {
                
                if (!StackTopInt(forCycles))
                {
                    instruction = instruction->arg;
                    stackPopInt(forCycles);
                }
                else
                {
                    StackTopDec(forCycles);
                }
                
                } break;
            
            // return control back to condition    
            case I_CYCLE_END:
                instruction = instruction->arg;
                instructionShift = false;
                break;
            
            // value == 0 -> skip scope; value != 0 enter scope
            case I_WHILE:
            case I_IF: {
                
                if (!m.data[m.ptr])
                {
                    instruction = instruction->arg;
                }
                
                } break;
            
            // value != 0 -> skip scope; value == 0 enter scope
            case I_WHILE_NOT: 
            case I_IF_NOT: {
                
                if (m.data[m.ptr])
                {
                    instruction = instruction->arg;
                }
                
                } break;
            
            // print value at current address as character
            case I_PUTCHAR:
                putchar(m.data[m.ptr]);
                break;
            
            // print value at current address as decimal number
            case I_PUTINT:
                printf("%u", m.data[m.ptr]);
                break;
                
            // print string starting at current address
            case I_PUTSTRING: {
                
                char c;
                
                while ((c = m.data[m.ptr]) != 0)
                {
                    putchar(c);
                    
                    if (m.ptr == m.highest)
                    {
                        break;
                    }
                    else
                    {
                        ++(m.ptr);
                    }
                } 
                
                } break;
            
            // read char from standard input to current address, EOF == 0    
            case I_GETCHAR: {
                
                m.data[m.ptr] = getchar();

                if (m.data[m.ptr] == (unsigned int)EOF) // if EOF occured
                {
                    m.data[m.ptr] = 0;
                }
                
                } break;
            
            // push value from current address to stack
            case I_PUSH:
                code = stackPushInt(stack, m.data[m.ptr]);
                break;
            
            // pop top value from stack to current address
            case I_POP: {
                
                if (!StackEmpty(stack))
                {
                    m.data[m.ptr] = stackPopInt(stack);
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_GETTOP, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_GETTOP;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_GETTOP, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // copy top value from stack to current address
            case I_TOP:{
                
                if (!StackEmpty(stack))
                {
                    m.data[m.ptr] = StackTopInt(stack);
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_GETTOP, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_GETTOP;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_GETTOP, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
                
            // remove top value from stack
            case I_REMOVE: {
                
                if (!StackEmpty(stack))
                {
                    stackPopInt(stack);
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_REMOVE, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_REMOVE;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_REMOVE, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // add value from top of stack to value at current address
            case I_ADD: {
                
                if (!StackEmpty(stack))
                {
                    m.data[m.ptr] += StackTopInt(stack);
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_USETOP, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_USETOP;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_USETOP, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // subtract value from top of stack from value at current address
            case I_SUBTRACT: {
                
                if (!StackEmpty(stack))
                {
                    m.data[m.ptr] -= StackTopInt(stack);
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_USETOP, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_USETOP;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_USETOP, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // devide value at current address by value from top of stack
            case I_DEVIDE: {
                
                if (!StackEmpty(stack))
                {
                    if (!StackTopInt(stack))
                    {                           
                        code = ERR_DEVIDE_BY_ZERO;                      
                    }
                    else
                    {
                        m.data[m.ptr] /= StackTopInt(stack);       
                    }
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_USETOP, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_USETOP;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_USETOP, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // multiply value at current address by value from top of stack
            case I_MULTIPLY: {
                
                if (!StackEmpty(stack))
                {
                    m.data[m.ptr] *= StackTopInt(stack);
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_USETOP, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_USETOP;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_USETOP, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // save value at current address to 1 if stack is empty, to 0 otherwse
            case I_EMPTY:
                m.data[m.ptr] = StackEmpty(stack);
                break;
            
            // increment top value on stack
            case I_STACKINC: {
                
                if (!StackEmpty(stack))
                {
                    StackTopInc(stack);
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_EDIT, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_EDIT;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_EDIT, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // decrement top value on stack
            case I_STACKDEC: {
                
                if (!StackEmpty(stack))
                {
                    StackTopDec(stack);
                }
                else
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_EMPTYSTACK_EDIT, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_EMPTYSTACK_EDIT;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_EMPTYSTACK_EDIT, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                
                } break;
            
            // print given string
            case I_ECHO:
                printf("%s", (char*)instruction->arg);
                break;
            
            // save given string to current address
            case I_SAVE_STRING: {
                
                char* tmp = (char*)instruction->arg;
                unsigned int tmpLength = strlen(tmp);
                
                unsigned int origAddr = m.ptr;
                unsigned int targetAddr = origAddr+tmpLength;
                
                // "allocating" memory for string
                if (targetAddr > m.highest)
                {
                    if ((code = incPtr(&m, targetAddr-origAddr)) != 0)
                    {
                        break;
                    }
                }
                
                m.ptr = origAddr;
                
                // saving string
                for (unsigned int i = 0; i < tmpLength; ++i)
                {
                    m.data[m.ptr+i] = tmp[i];
                }
                
                // adding ending character
                m.data[targetAddr] = 0;
                
                } break;
            
            case I_DEBUG:

            	if (args->debug)
            	{
            		if (debugCore(&m, stack, instruction->line)) // terminate debugging == -1
	                {
	                	code = -1;
	                	debugEndMessage();
	                }
            	}

                break;
            
            // terminate interpreted program    
            case I_END:
                code = -1;
                break;
            
            // attempt to all given procedure
            case I_PROC_CALL: {
                
                if ((code = stackPushInstPtr(procCalls, instruction)) != 0)
                {
                    break;
                }
                
                unsigned int procedureID = *((unsigned char*)instruction->arg);
                err->line = instruction->line; // just in case
                
                if ((instruction = procedures[procedureID]) == NULL)
                {
                    err->tokenID = procedureID+'a';
                    code = ERR_PROC_UNDEF;
                }
                
                } break;
            
            // jump back to calling instruction
            case I_PROC_END:
                instruction = stackPopInstPtr(procCalls);
                break;
            // sort memory between two top adresses on stack to ascending order    
            case I_SORT: {
                
                if (stack->size < 2) // to few parameters (values on stack)
                {
                    if (args->pedantic)
                    {
                        warPrint (WARNING_SORT_FEW, args->sourceFileName, instruction->line, STYLE_ERROR);
                        code = -WARNING_SORT_FEW;
                        goto error;
                    }
                    else if (!args->suppress)
                    {
                        warPrint (WARNING_SORT_FEW, args->sourceFileName, instruction->line, STYLE_WARNING);
                    }
                }
                else // enought parameters
                {
                    unsigned int left, right;
                    
                    left = stackPopInt(stack);
                    right = stackPopInt(stack);
                    
                    /*
                     * Making sure that the lower and higher address are saved
                     * in correct variables.
                     */
                    if (left > right)
                    {
                        unsigned int tmp = left;
                        left = right;
                        right = tmp;
                    }
                    else if (left == right)
                    {
                        // only one item to "sort" -> unncessary to call sort
                        break;
                    }
                    
                    if (right > m.highest)
                    {
                        // attempt to sort "uninitalized" part of memory
                        if (args->pedantic)
                        {
                            warPrint (WARNING_SORT_UNITIALIZED, args->sourceFileName, instruction->line, STYLE_ERROR);
                            code = -WARNING_SORT_UNITIALIZED;
                            goto error;
                        }
                        else if (!args->suppress)
                        {
                            warPrint (WARNING_SORT_UNITIALIZED, args->sourceFileName, instruction->line, STYLE_WARNING);
                        }
                    }
                    else // everything ok
                    {
                        quicksort(m.data, left, right);
                    }
                }
                
                } break;
                
            default:
                break;
        }
        
        if (code)
        {
            break;
        }
        
        if (instructionShift)
        {
            instruction = instruction->next;
        }
        else
        {
            instructionShift = true;
        }
    }
    
    if (code == -1)
    {
        code = 0;
    }

    stackDispose(procCalls);
    stackDispose(forCycles);
    stackDispose(stack);
    
    MemoryDispose(m);
    
    return code;


    error:

        err->line = instruction->line;

        stackDispose(procCalls);
        stackDispose(forCycles);
        stackDispose(stack);
        
        MemoryDispose(m);

        return code-PEDANTIC_WARNING_CODE_DIFF;
}

/* End of interpreter.c */
