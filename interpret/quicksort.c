/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include "quicksort.h"

/*
 * Implementation of quicksort for array of unsigned integers using recursive 
 * alogorithm.
 * 
 * This implemenatiton sorts items to ascending order.
 */
void quicksort (unsigned int array[], unsigned int left, unsigned int right)
{
    unsigned int pseudoMedian, i, j, tmp;
    
    pseudoMedian = array[(left+right)/2];
    j = right;
    i = left;
    
    do { // sort array until i and j are equal / change sides
        
        /*
         * Move new right pointer to the left until value lower than or equal 
         * to pseudomedian is found.
         */
        while (j > left && array[j] > pseudoMedian) 
        {
            j--;
        }
        
        /*
         * Move new leftht pointer to the right until value higher than or 
         * equal to pseudomedian is found.
         */
        while (i < right && array[i] < pseudoMedian) 
        {
            i++;
        }
 
        // if there is higher value on the left than on the right, swap values
        if (i <= j)
        {
            tmp = array[i];
            array[i++] = array[j];
            array[j--] = tmp;
        }
        
    } while (i < j);
 
    // if there is unsorted subarray on the right, sort it
    if (i < right) 
    {
        quicksort(array, i, right);
    }
    
    // if there is unsorted subarray on the rledt, sort it
    if (j > left) 
    {
        quicksort(array, left, j);
    }
}

/* End of file quicksor.c */
