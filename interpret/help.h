/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef HELP_H_INCLUDED
#define HELP_H_INCLUDED

/*
 * Printing of help text.
 */
void helpPrint (void);

#endif

/* End of file help.h */
