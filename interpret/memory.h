/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef MEMORY_H_INCLUDED
#define MEMORY_H_INCLUDED

// memory size is always (2^n)-1
#define MEM_DEF_SIZE 7

// frees memory user by interpreted program's memory
#define MemoryDispose(memory) free(memory.data)

/*
 * Structure for memory of intepreted program - stores all importatnt memory
 * info: memory data(values), pointer to active variable in memory, current size
 * of allocated memory and highest ptr of yet used variable (where will ptr
 * jump if program goes from ptr == 0 to the left).
 */
typedef struct TMemory {
    unsigned int *data, ptr, size, highest;
} TMemory;

/*
 * Definiton of pointer to memory.
 */
typedef TMemory* TMemoryPtr;

/*
 * Memory initialization - alloc default size memory, save the size and set
 * pointer and highest pointer to zero.
 *
 * Returns zero on success, ERR_ code otherwise.
 */
int memoryInit (TMemoryPtr m);

/*
 * Increments pointer. If the ponter is new highest adress, initializes value to
 * zero and saves new highest adress. In case ptr is UINT_MAX, only wrap back to
 * zero. Otherwise realloc the memory.
 *
 * Returns zero on success, ERR_ code otherwise.
 */
int incPtr (TMemoryPtr m, unsigned int number);

/*
 * Decrement pointer. If called from ptr == 0, go to highest used adress.
 */
void decPtr (TMemoryPtr m, unsigned int number);

#endif

/* End of file memory.h */
