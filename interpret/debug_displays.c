/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <limits.h>

#include "debug_displays.h"

/*
 * Shows current state o interpreted's program stack in pseudo-graphic output.
 */ 
void displayStack (const TStackPtr stack)
{
    unsigned long long size = stack->size;

    printf("\n--------------------- Current stack state --------------------\n");

    if (stack->top == NULL)
    {
        printf ("                stack in empty - no items to show\n");
    }
    else
    {
        printf("[ address:  %10llu ] value: %10u <-- STACK TOP\n",
                --size, *((unsigned int*)stack->top->content));

        TStackItemPtr tmp = stack->top->next;

        while (tmp != NULL)
        {
            printf("[ address:  %10llu ] value: %10u\n",
                    --size, *((unsigned int*)tmp->content));
            tmp = tmp->next;
        }
    }

    printf("--------------------------------------------------------------\n\n");
}

/*
 * Shows current state o interpreted program's memory in pseudo-graphic output.
 */
void displayMemory (const TMemoryPtr m)
{
    printf("\n-------------------- Current memory state --------------------\n");

    for (unsigned int i = 0; i <= m->highest; ++i)
    {
        fprintf(stderr, "[ address:  %10u ] value: %10u", i, m->data[i]);

        if (i == m->ptr)
        {
            fprintf(stderr, " <-- CURRENT ADDRESS\n");
        }
        else
        {
            fprintf(stderr, "\n");
        }
    }

    printf("--------------------------------------------------------------\n\n");
}

/* End of debug_displays.c */
