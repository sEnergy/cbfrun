/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef INTERPRETER_H_INCLUDED
#define INTERPRETER_H_INCLUDED

#include "ic_type.h"
#include "arg_type.h"
#include "errors.h"

int interpreter (TInnerCodePtr ic, TInstructionPtr* procedures, TErrorDetailPtr err, TArgumentsPtr args);

#endif

/* End of interpreter.h */
