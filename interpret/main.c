/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "arg_process.h"
#include "core.h"
#include "help.h"
#include "errors.h"

int main (int argc, char *argv[])
{
    setbuf(stdout, 0);
    setbuf(stderr, 0);
    
    TArguments args;
    
    TErrorDetail errorDetail = { .line = 0, 
                                 .columb = 0, 
                                 .tokenID = 0,
                                 .data = NULL };
                                 
    int code = argumentProcess(argc, argv, &args);

    switch (code)
    {
        case CODE_HELP:
            helpPrint();
            code = 0;
            break;
            
        case CODE_INTERPRETER:
            code = interpretationCore(&args, &errorDetail);
            break;
            
        default:
            break;
    }

    if (code > 0) // error
    {
        errPrint(code, argv[1], &errorDetail);
    }
    else
    {
        code *= -1;
    }

    return code;
}

/* End of file main.c */
