/*******************************************************************************
 *
 * Project name:        cbfrun
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 *                      mail@marcel-fiala.eu
 * Encoding:            UTF-8
 *
*******************************************************************************/

#ifndef ARGUMENT_TYPE_H_INCLUDED
#define ARGUMENT_TYPE_H_INCLUDED

#include <stdbool.h>

/*
 * Structure that carries detailed info about processed interpreter arguments.
 */
typedef struct TArguments {
    char* sourceFileName;
    bool debug, pedantic, suppress;
} TArguments;

typedef TArguments* TArgumentsPtr;

#endif

/* End of file arg_type.h */
